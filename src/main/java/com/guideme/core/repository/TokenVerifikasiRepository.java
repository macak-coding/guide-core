package com.guideme.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.guideme.common.entity.Kota;
import com.guideme.common.entity.Notifikasi;
import com.guideme.common.entity.TokenVerifikasi;
import com.guideme.common.entity.UserDetail;

@Repository
public interface TokenVerifikasiRepository extends PagingAndSortingRepository<TokenVerifikasi, Integer>{
	public TokenVerifikasi findByTokenCodeAndStatus(String tokenCode, String status);
	
	@Modifying(clearAutomatically = true)
	@Query("update TokenVerifikasi tk set tk.status =:status where tk.tokenCode =:tokenCode")
	public void updateStatusByToken(@Param("tokenCode") String tokenCode,  @Param("status") String status);
	
}
