package com.guideme.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.guideme.common.entity.Kota;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.service.inf.KotaServiceInf;

@RestController
@RequestMapping("/kota")
public class KotaController {
	
	KotaServiceInf kotaServiceInf;
	
	@Autowired
	KotaController(KotaServiceInf kotaServiceInf){
		this.kotaServiceInf =kotaServiceInf;
	}
	
	@RequestMapping(value="/get-all", method=RequestMethod.GET)
	public Page<Kota> getAllKota(Pageable pageable){
		
		return kotaServiceInf.getAllKotaPageable(pageable);
	}
	
	@RequestMapping(value="/getKota/{namaKota}", method=RequestMethod.GET)
	public List<Kota> getKotaLike(@PathVariable("namaKota") String namaKota){
		
		return kotaServiceInf.getKotaLike(namaKota);
	}
	
	@RequestMapping(value="/saveKota", method=RequestMethod.POST)
	public StatusResponse saveKota(@RequestBody List<Kota> listKota){
		
		return kotaServiceInf.saveKota(listKota);
	}
}
