package com.guideme.core.repository;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.guideme.common.entity.Komentar;

public interface KomentarRepository extends PagingAndSortingRepository<Komentar, Integer>{
	
	@Query("from Komentar k "
			+ "where k.idDiskusi.idDiskusi = ?1")
	List<Komentar> findByDiskusi(int diskusi);
}
