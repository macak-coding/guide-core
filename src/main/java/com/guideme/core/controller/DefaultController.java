package com.guideme.core.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.guideme.core.service.inf.EmailServiceInf;

@RestController
public class DefaultController {
	
	@RequestMapping({"/",""})
	Map<String, String> defaultPage(){
		Map<String, String> result = new HashMap<String, String>();
		
		/* Cuman data dummy, nggak dipakai apa-apa wkwkwk */
		
		result.put("user", "administrator");
		result.put("email", "");
		result.put("plainPassword", "XZfJD94YT4cUyS+LCjMs3r$w");
		result.put("serviceStatus", "OK");
		
		return result;
	}
}
