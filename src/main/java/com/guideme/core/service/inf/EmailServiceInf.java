package com.guideme.core.service.inf;

import java.util.Map;

import com.guideme.common.model.EmailModel;

public interface EmailServiceInf {
	public void sendPlainEmail(String from, String to, String subject, String body);
	public void sendHtmlEmail(EmailModel email, Map<String, String> param);
}
