package com.guideme.core.controller;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.guideme.common.entity.JalanBareng;
import com.guideme.common.entity.JalanBarengAnggota;
import com.guideme.common.entity.Kota;
import com.guideme.common.entity.User;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.service.inf.JalanBarengServiceInf;

@RestController
@RequestMapping("/jalanBareng")
public class JalanBarengController {
	Logger logger = Logger.getLogger(JalanBarengController.class);
	
	JalanBarengServiceInf jalanBarengService;
	
	@Autowired
	public JalanBarengController(JalanBarengServiceInf jalanBarengService){
		this.jalanBarengService = jalanBarengService;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public StatusResponse<String> createJalanBareng(@RequestBody JalanBareng jalanBareng){
		StatusResponse statusResponse = jalanBarengService.createJalanBareng(jalanBareng);
		return statusResponse;
	}
	
	@RequestMapping(value="/selesai", method = RequestMethod.PUT)
	public StatusResponse<String> selesaiJalanBareng(@RequestBody JalanBareng jalanBareng){
		StatusResponse statusResponse = jalanBarengService.selesaiJalanBareng(jalanBareng);
		return statusResponse;
	}
	
	@RequestMapping(value="/tokenId/{tokenId}", method = RequestMethod.GET)
	public StatusResponse<String> getByTokenId(@PathVariable("tokenId") String tokenId){
		StatusResponse statusResponse = jalanBarengService.getByTokenID(tokenId);
		return statusResponse;
	}
	
	@RequestMapping(value="/anggota", method = RequestMethod.POST)
	public StatusResponse<String> anggota(@RequestBody JalanBarengAnggota jalanBarengAnggota){
		StatusResponse statusResponse = jalanBarengService.addAnggotaJalanBareng(jalanBarengAnggota);
		return statusResponse;
	}
	
	@RequestMapping(value="/anggota/accept/{idJalanBareng}/{idAnggota}", method = RequestMethod.PUT)
	public StatusResponse<String> acceptAnggota(@PathVariable("idAnggota") int idAnggota, @PathVariable("idJalanBareng") int idJalanBareng){
		StatusResponse statusResponse = jalanBarengService.acceptAngotaJalanBareng(idAnggota, idJalanBareng);
		return statusResponse;
	}
	
	@RequestMapping(value="/anggota/reject/{idJalanBareng}/{idAnggota}", method = RequestMethod.PUT)
	public StatusResponse<String> rejectAnggota(@PathVariable("idAnggota") int idAnggota, @PathVariable("idJalanBareng") int idJalanBareng){
		StatusResponse statusResponse = jalanBarengService.rejectAngotaJalanBareng(idAnggota, idJalanBareng);
		return statusResponse;
	}
	
	@RequestMapping(value="/anggota/kick/{idJalanBareng}/{idAnggota}", method = RequestMethod.PUT)
	public StatusResponse<String> kickAnggota(@PathVariable("idAnggota") int idAnggota, @PathVariable("idJalanBareng") int idJalanBareng){
		StatusResponse statusResponse = jalanBarengService.kickAngotaJalanBareng(idAnggota, idJalanBareng); 
		return statusResponse;
	}
	
	@RequestMapping(value="/anggota/{username}/{token}", method = RequestMethod.GET)
	public StatusResponse<String> getAnggotaByUsername(@PathVariable("username") String username, @PathVariable("token") String token){
		StatusResponse statusResponse = jalanBarengService.getAnggotaByUsername(username, token);
		return statusResponse;
	}
	
	@RequestMapping(value="/getAllPageable", method = RequestMethod.GET)
	public StatusResponse getAllPageable(Pageable pageable){
		StatusResponse statusResponse = jalanBarengService.getAllPage(pageable);
		return statusResponse;
	}
	
	@RequestMapping(value="/getAllByKotaTujuan/{kotaTujuan}", method = RequestMethod.GET)
	public StatusResponse getAllByNamaKota(@PathVariable("kotaTujuan") String kotaTujuan, Pageable pageable){
		StatusResponse statusResponse = jalanBarengService.getAllByKotaPage(kotaTujuan, pageable);
		return statusResponse;
	}
	
}
