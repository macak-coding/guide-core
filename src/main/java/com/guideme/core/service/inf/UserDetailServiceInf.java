package com.guideme.core.service.inf;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.SearchCriteria;
import com.guideme.common.model.StatusResponse;

public interface UserDetailServiceInf {
	public StatusResponse getUserDetailByUsername(String username);
	public StatusResponse getUserDetailById(int idUserDetail);
	public StatusResponse saveUserDetail(UserDetail userDetail);
	public StatusResponse updateUserDetail(UserDetail userDetail);
	public StatusResponse updateUserStatus(int idUserDetail, String newStatus);
	public StatusResponse getAllUserDetailPageable(Pageable pageable);
	public StatusResponse getAllUserDetailForAdmin();
	public StatusResponse getUserDetailByCriteria(Pageable pageable, String searchString);
	public StatusResponse updateFotoProfilUrl(int idUserDetail, String url);
	public StatusResponse updateFotoIdentitas(UserDetail userDetail, int flagIdentitas);
}
