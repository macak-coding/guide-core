package com.guideme.core.service.inf;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.guideme.common.entity.Kota;
import com.guideme.common.model.StatusResponse;

public interface KotaServiceInf {
	List getAllKota();
	Page<Kota> getAllKotaPageable(Pageable pageable);
	List<Kota> getKotaLike(String nameKota);
	StatusResponse saveKota(List<Kota> kota);
}
