package com.guideme.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.guideme.common.entity.Diskusi;

public interface DiskusiRepository extends PagingAndSortingRepository<Diskusi, Integer>{
	@Query("from Diskusi d "
			+ "where d.idPengirim.idUserDetail = ?1 ")
	public Page<Diskusi> findByIdPengirim(int idUserDetail, Pageable p);
	
	@Query("from Diskusi d "
			+ "where d.idPenerima.idUserDetail = ?1 ")
	public Page<Diskusi> findByIdPenerima(int idUserDetail, Pageable p);
	
}
