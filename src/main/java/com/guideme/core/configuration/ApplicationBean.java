package com.guideme.core.configuration;

import java.security.SecureRandom;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;

@Configuration
public class ApplicationBean {
	
	@Bean
    public FreeMarkerConfigurationFactoryBean getFreemarkerConfiguration() {
        FreeMarkerConfigurationFactoryBean freemarkerConfiguration = new FreeMarkerConfigurationFactoryBean();
        freemarkerConfiguration.setTemplateLoaderPath("classpath:templates/");
        return freemarkerConfiguration;
    }
	
	@Bean
	public ObjectMapper getObjectMapper(){
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new Hibernate4Module());
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		return mapper;
	}
	
	@Bean
	public SecureRandom secureRandom(){
		return new SecureRandom();
	}
}
