package com.guideme.core.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class EmailAlreadyExistException extends Exception{
	public EmailAlreadyExistException(String message) {
		super(message);
	}
}
