package com.guideme.core.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guideme.common.entity.Transaksi;
import com.guideme.common.entity.ViewDtTransaksi;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.repository.TransaksiRepository;
import com.guideme.core.repository.ViewDtTransaksiRepository;
import com.guideme.core.service.inf.TransaksiServiceInf;

@Service
@Transactional(readOnly=true)
public class TransaksiServiceImpl implements TransaksiServiceInf{
	Logger logger = Logger.getLogger(TransaksiServiceImpl.class);
	
	TransaksiRepository transaksiRepo;
	ViewDtTransaksiRepository viewDtTransaksiRepo;
	
	@Autowired
	TransaksiServiceImpl(TransaksiRepository transaksiRepo, ViewDtTransaksiRepository viewDtTransaksiRepo){
		this.transaksiRepo = transaksiRepo;
		this.viewDtTransaksiRepo = viewDtTransaksiRepo;
	}
	
	@Override
	public Page<Transaksi> getListTransaksiPageable(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=false)
	public StatusResponse updateBuktiTransaksi(int idTransaksi, String statusPembayaran, String urlBuktiPembayaran) {
		StatusResponse statusResponse = new StatusResponse<>("success", "Berhasil update url bukti pembayaran dan status perjalanan menjadi: "+ statusPembayaran);
		try {
			transaksiRepo.updateBuktiPembayaran(idTransaksi, statusPembayaran, urlBuktiPembayaran);
		} catch (Exception e) {
			e.printStackTrace();
			statusResponse = new StatusResponse<>("failed", "Gagal update url bukti pembayaran dan status perjalanan menjadi: "+ statusPembayaran+" id trx: "+idTransaksi);
		}
		
		return statusResponse;
	}

	@Override
	public StatusResponse getByIdTransaksi(String kodeTransaksi, String username) {
		StatusResponse statusResponse = new StatusResponse<>("success", "Berhasil mendapatkan transaksi dengan id: "+kodeTransaksi);
		Transaksi transaksi = null;
		try {
			transaksi = transaksiRepo.findByKodeTransaksiAndCreatedBy(kodeTransaksi, username);
			statusResponse.setReturnObject(transaksi);
		} catch (Exception e) {
			e.printStackTrace();
			statusResponse = new StatusResponse<>("failed", "Gagal mendapatkan transaksi dengan id: "+kodeTransaksi);
		}
		
		return statusResponse;
	}
	
	@Override
	public StatusResponse getByIdPerjalanan(int idPerjalanan) {
		StatusResponse statusResponse;
		Transaksi transaksi = null;
		try {
			transaksi = transaksiRepo.findByIdPerjalanan(idPerjalanan);
			statusResponse = new StatusResponse<>("success", "Berhasil mendapatkan transaksi dengan idPerjalanan: "+idPerjalanan, transaksi);
		} catch (Exception e) {
			e.printStackTrace();
			statusResponse = new StatusResponse<>("failed", "Gagal mendapatkan transaksi dengan idPerjalanan: "+idPerjalanan);
		}
		
		return statusResponse;
	}

	@Override
	public StatusResponse getListTransaksiForAdmin() {
		Iterable<ViewDtTransaksi> listTransaksi = null;
		try {
			listTransaksi = viewDtTransaksiRepo.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return new StatusResponse("failed", "Gagal mendapatkan data transaksi untuk admin", e.getMessage());
		}
		
		return new StatusResponse("success", "Berhasil mendapatkan data transaksi untuk admin", listTransaksi);
	}

}
