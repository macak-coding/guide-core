package com.guideme.core.service.inf;

import com.guideme.common.entity.RatingPerjalanan;
import com.guideme.common.entity.Review;
import com.guideme.common.model.StatusResponse;

public interface ReviewServiceInf {
	StatusResponse saveReviewGuide(Review review, RatingPerjalanan ratingPerjalanan);
	StatusResponse getAllReview();
}
