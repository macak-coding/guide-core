package com.guideme.core.controller;


import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.guideme.common.entity.Kota;
import com.guideme.common.entity.User;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.SearchCriteria;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.service.exception.EmailAlreadyExistException;
import com.guideme.core.service.exception.UserAlreadyExistException;
import com.guideme.core.service.inf.UserDetailServiceInf;

@RestController
@RequestMapping("/user-detail")
public class UserDetailController {
	Logger logger = Logger.getLogger(UserDetailController.class);
	
	@Autowired
	UserDetailServiceInf userDetailService;
	
	@RequestMapping(value="/{username}",method = RequestMethod.GET)
	public ResponseEntity<?> getUserDetailByUsername(@PathVariable(value="username") String username) {
		StatusResponse<UserDetail> statusResponse = null;
		
		try {
			statusResponse = userDetailService.getUserDetailByUsername(username);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResponseEntity.ok(new StatusResponse("Failed", "Error undefined.",e.getMessage()));
		}
		
		return ResponseEntity.ok(statusResponse);
	}
	
	@RequestMapping(value={"","/"}, method = RequestMethod.POST)
	public ResponseEntity<StatusResponse> saveUserDetail(@RequestBody UserDetail userDetail){
			StatusResponse statusResponse = null;
			
			try {
				statusResponse = userDetailService.saveUserDetail(userDetail);
			} catch (Exception e) {
				logger.error(e.getMessage());
				e.printStackTrace();
				return ResponseEntity.ok(new StatusResponse("Failed", "Error undefined.",e.getMessage()));
			}
			logger.info("Saving user detail "+userDetail.getNamaPanggilan()+" "+statusResponse.getStatus());
			return ResponseEntity.ok(statusResponse);
	}
	
	@RequestMapping(value={"","/"}, method = RequestMethod.PUT)
	public StatusResponse updateUserDetail(@RequestBody UserDetail userDetail){
			logger.debug("[CORE CONTROLLER] Udating user, idUserDetail: "+userDetail.getIdUserDetail()+", username: "+userDetail.getIdUser().getUsername());
			StatusResponse statusResponse = null;
			
			try {
				statusResponse = userDetailService.updateUserDetail(userDetail);
			} catch (Exception e) {
				logger.error(e.getMessage());
				e.printStackTrace();
				return new StatusResponse("Failed", "Error update user, idUserDetail:"+userDetail.getIdUserDetail(),e.getMessage());
			}
			logger.info("Saving user detail "+userDetail.getNamaPanggilan()+" "+statusResponse.getStatus());
			return statusResponse;
	}
	
	@RequestMapping(value={"/update-status-user"}, method = RequestMethod.PUT)
	public ResponseEntity<StatusResponse> updateStatusUser(@RequestBody UserDetail userDetail){
			StatusResponse statusResponse = null;
			
			try {
				statusResponse = userDetailService.updateUserStatus(userDetail.getIdUserDetail(), userDetail.getStatusUser());
			} catch (Exception e) {
				logger.error(e.getMessage());
				return ResponseEntity.ok(new StatusResponse("failed", "Gagal update user.", e.getMessage()));
			}
			logger.info("Update status user berhasil.");
			return ResponseEntity.ok(statusResponse);
	}
	
	@RequestMapping(value={"/findAll"}, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<StatusResponse> getAll(Pageable pageable){
		StatusResponse statusResponse = null;
		Page<UserDetail> page = null;
		try {
			statusResponse = userDetailService.getAllUserDetailPageable(pageable);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResponseEntity.ok(new StatusResponse("failed", "Gagal mendapatkan data user.", e.getMessage()));
		}
		return ResponseEntity.ok(statusResponse);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public StatusResponse getAllUserDetail(){
		StatusResponse statusResponse = null;
		try {
			statusResponse = userDetailService.getAllUserDetailForAdmin();
		} catch (Exception e) {
			e.printStackTrace();
			return new StatusResponse("failed", "Gagal mendapatkan data user.", e.getMessage());
		}
		return statusResponse;
	}
	
	@RequestMapping(value={"/findByCriteria"}, method = RequestMethod.GET)
	@ResponseBody
	public Page<UserDetail> getByCriteria(@RequestParam(value="criteria") String criteria, Pageable pageable){
		StatusResponse<Page<UserDetail>> statusResponse = null;
		Page<UserDetail> page = null;
		try {
			statusResponse = userDetailService.getUserDetailByCriteria(pageable, criteria);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return (Page<UserDetail>) statusResponse.getReturnObject();
	}
	
	@RequestMapping(value={"/findByIdUserDetail"}, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<StatusResponse> getByIdUserDetail(@RequestParam(value="idUserDetail") int idUserDetail){
		StatusResponse statusResponse = null;
		try {
			statusResponse = userDetailService.getUserDetailById(idUserDetail);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResponseEntity.ok(new StatusResponse("failed", "[CORE] Gagal mendapatkan data user.", e.getMessage()));
		}
		return ResponseEntity.ok(statusResponse);
	}
	
	@RequestMapping(value={"/updateFotoProfilUrl"}, method = RequestMethod.PUT)
	public ResponseEntity<StatusResponse> updateFotoProfilUrl(@RequestBody UserDetail userDetail){
			StatusResponse statusResponse = null;
			
			try {
				statusResponse = userDetailService.updateFotoProfilUrl(userDetail.getIdUserDetail(), userDetail.getUrlFoto());
			} catch (Exception e) {
				logger.error(e.getMessage());
				return ResponseEntity.ok(new StatusResponse("failed", "Gagal update foto profil user.", e.getMessage()));
			}
			logger.info("Update foto profil berhasil");
			return ResponseEntity.ok(statusResponse);
	}
	
	@RequestMapping(value={"/updateFotoIdentitasUrl"}, method = RequestMethod.PUT)
	public ResponseEntity<StatusResponse> updateFotoIdentitasUrl(@RequestBody UserDetail userDetail){
			StatusResponse statusResponse = null;
			logger.debug("[CONTROLLER CORE] Id user: "+userDetail.getIdUserDetail());
			logger.debug("[CONTROLLER CORE] Identitas Url1: "+userDetail.getUrlIdentitas()+" Url2: "+userDetail.getUrlIdentitas2());
			logger.debug("[CONTROLLER CORE] Identitas1: "+userDetail.getJenisIdentitas()+" Identitas2: "+userDetail.getJenisIdentitas2());
			
			try {
				if(null != userDetail.getUrlIdentitas())
					statusResponse = userDetailService.updateFotoIdentitas(userDetail,1);
				else
					statusResponse = userDetailService.updateFotoIdentitas(userDetail,2);
			} catch (Exception e) {
				logger.error(e.getMessage());
				return ResponseEntity.ok(new StatusResponse("failed", "Gagal update foto profil user.", e.getMessage()));
			}
			logger.info("Update foto profil berhasil");
			return ResponseEntity.ok(statusResponse);
	}
}
