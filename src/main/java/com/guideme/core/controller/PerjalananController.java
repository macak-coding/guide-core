package com.guideme.core.controller;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.guideme.common.entity.Perjalanan;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.service.inf.PerjalananServiceInf;

@RestController
@RequestMapping("/perjalanan")
public class PerjalananController {
	
	private static final Logger logger = Logger.getLogger(PerjalananController.class);

	@Autowired PerjalananServiceInf perjalananService;
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<StatusResponse> savePerjalanan(@RequestBody Perjalanan perjalanan){
		StatusResponse statusResponse = null;
		
		try {
			statusResponse = perjalananService.createPerjalanan(perjalanan);
			return ResponseEntity.ok(statusResponse);
		} catch (Exception e) {
			return ResponseEntity.ok(new StatusResponse("failed", "[CORE] Gagal melakukan save", e.getMessage()));
		}
	}
	
	@RequestMapping(value="/getPerjalananForReview", method=RequestMethod.GET)
	public ResponseEntity<StatusResponse> getPerjalananForReview(
			@RequestParam(value = "idGuide", required = false) int idGuide,
			@RequestParam(value = "idTraveler", required = false) int idTraveler,
			@RequestParam(value = "statusPerjalanan", required = false) String statusPerjalanan){
		StatusResponse statusResponse = null;
		
		try {
			statusResponse = perjalananService.getPerjalananForReview(idGuide, idTraveler, statusPerjalanan);
			return ResponseEntity.ok(statusResponse);
		} catch (Exception e) {
			return ResponseEntity.ok(new StatusResponse("failed", "Gagal mendapatkan data perjalanan [CORE]", e.getMessage()));
		}
	}
	
	@RequestMapping(value="/getPerjalananById", method=RequestMethod.GET)
	public ResponseEntity<?> getPerjalananByIdGuide(
						 @RequestParam(value = "idGuide", required = true) int idGuide){
		StatusResponse<List<Perjalanan>> statusResponse = null;
		
		try {
			statusResponse = perjalananService.getPerjalananByIdGuide(idGuide);
			return ResponseEntity.ok(statusResponse);
		} catch (Exception e) {
			return ResponseEntity.ok(new StatusResponse("failed", "Gagal mendapatkan data perjalanan [CORE]", e.getMessage()));
		}
	}
	
	@RequestMapping(value="/updateStatusPerjalanan", method=RequestMethod.PUT)
	public ResponseEntity updateStatusPerjalanan(@RequestBody Map<String, Object> parameter){
		StatusResponse statusResponse = null;
		
		Integer idPerjalanan = (Integer) parameter.get("idPerjalanan");
		String statusPerjalanan = (String) parameter.get("statusPerjalanan");
		String pesan = (String) parameter.get("pesan");
		
		try {
			statusResponse = perjalananService.updateStatusPerjalanan(idPerjalanan, statusPerjalanan, pesan);
			return ResponseEntity.ok(statusResponse);
		} catch (Exception e) {
			logger.error("[CONTROLLER CORE] Gagal update status perjalanan");
			return ResponseEntity.ok(new StatusResponse("failed", "[CONTROLLER CORE] Gagal update status perjalanan", e.getMessage()));
		}
	}
	
	@RequestMapping(value="/getPerjalananByIdGuidePage/{idGuide}", method=RequestMethod.GET)
	public ResponseEntity getPerjalananByIdGuide(
						 @PathVariable(value = "idGuide") int idGuide, Pageable pageable){
		logger.debug("[CONTROLLER CORE] Meminta data perjalanan dengan id guide: "+idGuide);
		StatusResponse statusResponse = null;
		
		try {
			statusResponse = perjalananService.getPerjalananByIdGuidePage(idGuide, pageable);
			return ResponseEntity.ok((Page<Perjalanan>) statusResponse.getReturnObject());
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResponseEntity.ok("ERROR CORE");
		}
	}
	
	@RequestMapping(value="/getPerjalananByIdTravelerPage/{idTraveler}", method=RequestMethod.GET)
	public ResponseEntity getPerjalananByidTraveler(
						 @PathVariable(value = "idTraveler") int idTraveler, Pageable pageable){
		logger.debug("[CONTROLLER CORE] Meminta data perjalanan dengan id traveler: "+idTraveler);
		StatusResponse statusResponse = null;
		
		try {
			statusResponse = perjalananService.getPerjalananByIdTravelerPage(idTraveler, pageable);
			return ResponseEntity.ok((Page<Perjalanan>) statusResponse.getReturnObject());
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResponseEntity.ok("ERROR CORE");
		}
	}
	
	@RequestMapping(value="/getPerjalananByIdPerjalanan/{idPerjalanan}", method=RequestMethod.GET)
	public StatusResponse getPerjalananByidPerjalanan(
						 @PathVariable(value = "idPerjalanan") int idPerjalanan){
		
		logger.debug("[CONTROLLER CORE] Meminta data perjalanan dengan id traveler: "+idPerjalanan);
		StatusResponse statusResponse = null;
		
		statusResponse = perjalananService.getPerjalananByIdPerjalanan(idPerjalanan);
		return statusResponse;
	}
	
	@RequestMapping(value="/test", method=RequestMethod.GET)
	public StatusResponse test(){
		
		StatusResponse statusResponse = null;
		
		statusResponse = perjalananService.test();
		return statusResponse;
	}
}
