package com.guideme.core.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.guideme.common.entity.Notifikasi;
import com.guideme.common.entity.Perjalanan;

@Repository
public interface NotifikasiRepository extends PagingAndSortingRepository<Notifikasi, Integer>{
	
	@Query("select notif from Notifikasi notif where notif.idPenerima.idUserDetail = :idPenerima order by notif.createdOn desc")
	public List<Notifikasi> findByIdPenerima(@Param("idPenerima") int idPenerima);
	
	@Query("select notif from Notifikasi notif where notif.idPenerima.idUserDetail = :idPenerima order by notif.createdOn desc, notif.idNotifikasi desc")
	public Page<Notifikasi> findByIdPenerimaPageable(@Param("idPenerima") int idPenerima, Pageable pageable);
	
	@Query("select notif from Notifikasi notif where notif.idPenerima.idUserDetail = :idPengirim order by notif.createdOn desc")
	public List<Notifikasi> findByIdPengirim(@Param("idPengirim") int idPengirim);
	
	@Query("select notif from Notifikasi notif where notif.idNotifikasi = :idNotifikasi and notif.idPenerima.idUserDetail = :idPenerima order by notif.createdOn desc")
	public Notifikasi findByIdNotifikasiAndIdPenerima(@Param("idNotifikasi") int idNotifikasi, @Param("idPenerima") int idPenerima);
	
	@Query("select notif from Notifikasi notif where notif.idPerjalanan.idPerjalanan = :idPerjalanan order by notif.createdOn desc")
	public Page<Notifikasi> findByIdPerjalanan(@Param("idPerjalanan") int idPerjalanan, Pageable pageable);
}
