package com.guideme.core.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guideme.common.constanta.Constanta;
import com.guideme.common.entity.Notifikasi;
import com.guideme.common.entity.NotifikasiTipe;
import com.guideme.common.entity.Perjalanan;
import com.guideme.common.entity.RatingDetail;
import com.guideme.common.entity.RatingPerjalanan;
import com.guideme.common.entity.Review;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.repository.NotifikasiRepository;
import com.guideme.core.repository.PerjalananRepository;
import com.guideme.core.repository.RatingDetailRepository;
import com.guideme.core.repository.RatingPerjalananRepository;
import com.guideme.core.repository.ReviewRepository;
import com.guideme.core.service.inf.ReviewServiceInf;

@Service
@Transactional(readOnly=true)
public class ReviewServiceImpl implements ReviewServiceInf{
	Logger logger = Logger.getLogger(ReviewServiceImpl.class);
	
	ReviewRepository reviewRepository;
	RatingPerjalananRepository ratingPerjalananRepository;
	PerjalananRepository perjalananRepository;
	NotifikasiRepository notifikasiRepository;
	RatingDetailRepository ratingDetailRepo;
	
	@Autowired
	public ReviewServiceImpl(ReviewRepository reviewRepository, RatingPerjalananRepository ratingPerjalananRepository,
					  PerjalananRepository perjalananRepository, NotifikasiRepository notifikasiRepository,
					  RatingDetailRepository ratingDetailRepo){
		
		this.reviewRepository = reviewRepository;
		this.ratingPerjalananRepository = ratingPerjalananRepository;
		this.perjalananRepository = perjalananRepository;
		this.notifikasiRepository = notifikasiRepository;
		this.ratingDetailRepo = ratingDetailRepo;
	}
	
	@Override
	@Transactional(readOnly=false)
	public StatusResponse saveReviewGuide(Review review, RatingPerjalanan ratingPerjalanan) {
		Date now = new Date();
		review.setCreatedOn(now);
		
		Perjalanan perjalanan = perjalananRepository.findByIdRatingPerjalananAndIdReview(ratingPerjalanan, review);
		RatingDetail currentRating = perjalanan.getIdGuide().getIdRatingDetail();
		RatingDetail calculatedRating = recalculateRating(currentRating, ratingPerjalanan);
		
		Notifikasi notifikasi = new Notifikasi();
		notifikasi.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_MENDAPAT_REVIEW));
		notifikasi.setIdPerjalanan(perjalanan);
		notifikasi.setIdPengirim(perjalanan.getIdTraveler());
		notifikasi.setIdPenerima(perjalanan.getIdGuide());
		notifikasi.setPesan(Constanta.NOTIFIKASI_PESAN_MENDAPAT_REVIEW);
		notifikasi.setCreatedOn(now);
		
		
		try {
			reviewRepository.save(review);
			ratingPerjalananRepository.save(ratingPerjalanan);
			notifikasiRepository.save(notifikasi);
			ratingDetailRepo.save(calculatedRating);
			
			return new StatusResponse("success","Berhasil menyimpan review.");
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return new StatusResponse("failed","Gagal menyimpan review",e.getMessage());
		}
	}

	@Override
	public StatusResponse<Iterable<Review>> getAllReview() {
		Iterable<Review> listReview = null;
		
		try {
			listReview = reviewRepository.findAll();
			return new StatusResponse("success","Berhasil mendapatkan data review",listReview);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return new StatusResponse("failed","Gagal mendapatkan data review",e.getMessage());
		}
	}
	
	private RatingDetail recalculateRating(RatingDetail currentRating, RatingPerjalanan newRating) {
		
		float currentAlternatif = currentRating.getAlternatif();
		float currentKebersihan = currentRating.getKebersihan();
		float currentKesopanan = currentRating.getKesopanan();
		float currentWaktu = currentRating.getKetepatanWaktu();
		float currentNavigasi = currentRating.getNavigasi();
		float currentPelayanan = currentRating.getPelayanan();
		float currentPenjadwalan = currentRating.getPenjadwalan();
		float currentTotalRating = currentRating.getTotalRating();
		
		float newAlternatif = newRating.getAlternatif();
		float newKebersihan = newRating.getKebersihan();
		float newKesopanan = newRating.getKesopanan();
		float newWaktu = newRating.getKetepatanWaktu();
		float newNavigasi = newRating.getNavigasi();
		float newPelayanan = newRating.getPelayanan();
		float newPenjadwalan = newRating.getPenjadwalan();
		float newTotalRating = newRating.getTotalRating();
		
		float calculatedAlternatif = (currentAlternatif + newAlternatif) / 2;
		float calculatedKebersihan = (currentKebersihan + newKebersihan) / 2;
		float calculatedKesopanan = (currentKesopanan + newKesopanan) / 2;
		float calculatedWaktu = (currentWaktu + newWaktu) / 2;
		float calculatedNavigasi = (currentNavigasi + newNavigasi) / 2;
		float calculatedPelayanan = (currentPelayanan + newPelayanan) / 2;
		float calculatedPenjadwalan = (currentPenjadwalan + newPenjadwalan) / 2;
		float calculatedTotalRating =(currentTotalRating + newTotalRating) / 2;
		
		/*
		 * Perhitungan rating di core, kalo pertamakali dapet rating harus langsung masukin, bukan dibagi dua.
		 */
		
		if(currentRating.getTotalRating() == 0){
			return new RatingDetail(currentRating.getIdRatingDetail(), newAlternatif, newKebersihan, newKesopanan, newWaktu, newNavigasi, newPelayanan, newPenjadwalan, newTotalRating);
		}else{
			return new RatingDetail(currentRating.getIdRatingDetail(), calculatedAlternatif, calculatedKebersihan, calculatedKesopanan, calculatedWaktu, calculatedNavigasi, calculatedPelayanan, calculatedPenjadwalan, calculatedTotalRating);
		}
		
	}

}
