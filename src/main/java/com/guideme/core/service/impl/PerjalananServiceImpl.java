package com.guideme.core.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guideme.common.constanta.Constanta;
import com.guideme.common.entity.Notifikasi;
import com.guideme.common.entity.NotifikasiTimeline;
import com.guideme.common.entity.NotifikasiTipe;
import com.guideme.common.entity.Perjalanan;
import com.guideme.common.entity.Transaksi;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.repository.NotifikasiRepository;
import com.guideme.core.repository.NotifikasiTimelineRepository;
import com.guideme.core.repository.PerjalananRepository;
import com.guideme.core.repository.TransaksiRepository;
import com.guideme.core.service.inf.PerjalananServiceInf;

@Service
@Transactional(readOnly=true)
public class PerjalananServiceImpl implements PerjalananServiceInf{
	Logger logger = Logger.getLogger(PerjalananServiceImpl.class);
	
	private PerjalananRepository perjalananRepo;
	private NotifikasiRepository notifikasiRepo;
	private NotifikasiTimelineRepository notifikasiTimelineRepo;
	private TransaksiRepository transaksiRepo;
	private SecureRandom secureRandom;
	
	@Autowired
	PerjalananServiceImpl(PerjalananRepository perjalananRepo, NotifikasiRepository notifikasiRepo,
			NotifikasiTimelineRepository notifikasiTimelineRepo, TransaksiRepository transaksiRepo,
			SecureRandom secureRandom){
		
		this.perjalananRepo = perjalananRepo;
		this.notifikasiRepo = notifikasiRepo;
		this.notifikasiTimelineRepo = notifikasiTimelineRepo;
		this.transaksiRepo = transaksiRepo;
		this.secureRandom = secureRandom;
	}
	
	@Override
	@Transactional(readOnly=false)
	public StatusResponse createPerjalanan(Perjalanan perjalanan) {
		Notifikasi notifikasi = null;
		Notifikasi notifikasiTraveler = null;
		NotifikasiTipe notifTipe = null;
		
		Date now = new Date();
		notifTipe = new NotifikasiTipe(Constanta.NOTIFIKASI_REQ_PERJALANAN);
		
		NotifikasiTimeline notifTimelinePerjalananBaru = new NotifikasiTimeline();
		Notifikasi notifPembayaran = null;
		
		NotifikasiTimeline notifTimelinePembayaran = null;
		Transaksi transaksi = null;
		
		logger.debug("####### [REQ PERJALANAN] Menyimpan notifikasi request perjalanan dari "+perjalanan.getIdGuide()+" ke "+perjalanan.getIdTraveler());
		notifikasi = new Notifikasi(now, perjalanan.getIdGuide(), perjalanan.getIdTraveler(), perjalanan.getPesan(), 0, notifTipe, perjalanan);
		notifikasiTraveler = new Notifikasi(now, perjalanan.getIdTraveler(), perjalanan.getIdTraveler(), perjalanan.getPesan(), 0, new NotifikasiTipe("009"), perjalanan);
		try {
			Perjalanan savedPerjalanan = perjalananRepo.save(perjalanan);
			notifikasiRepo.save(notifikasi);
			notifikasiRepo.save(notifikasiTraveler);
			
			
			/**
			 * Simpan notifikasi untuk timeline status di masing2 perjalanan yang ada di dashboard
			 */
			
			notifTimelinePerjalananBaru.setCreatedOn(now);
			notifTimelinePerjalananBaru.setIdNotifikasiTipe(notifTipe);
			notifTimelinePerjalananBaru.setIdPerjalanan(savedPerjalanan.getIdPerjalanan());
			notifTimelinePerjalananBaru.setPesan(savedPerjalanan.getPesan());
			notifTimelinePerjalananBaru.setRolePenerima(perjalanan.getIdGuide().getIdUser().getUserRoles());
			notifTimelinePerjalananBaru.setRolePengirim(perjalanan.getIdTraveler().getIdUser().getUserRoles());
			
			notifikasiTimelineRepo.save(notifTimelinePerjalananBaru);
			
			notifPembayaran = new Notifikasi();
			notifPembayaran.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_PEMBAYARAN));
			notifPembayaran.setIdPengirim(perjalanan.getIdGuide());
			notifPembayaran.setIdPenerima(perjalanan.getIdTraveler());
			notifPembayaran.setIdPerjalanan(perjalanan);
			notifPembayaran.setPesan(Constanta.NOTIFIKASI_PESAN_PEMBAYARAN);
			notifPembayaran.setCreatedOn(new Date());
			
			notifikasiRepo.save(notifPembayaran);
			
			DateTime trxDate = new DateTime();
			int random  = Math.abs(secureRandom.nextInt(99999)); // batas atas 99999
			
			// Generate transaction code
			StringBuffer trxCode = new StringBuffer("TR");
			trxCode.append(random);
			trxCode.append("-");
			trxCode.append(trxDate.getDayOfYear());
			trxCode.append(trxDate.getYear());
			trxCode.append("PJL");
			
			logger.info("KODE TRANSAKSI: "+trxCode);
			
			transaksi = new Transaksi();
			transaksi.setKodeTransaksi(trxCode.toString());
			transaksi.setUrlBuktiPembayaran("verifikasi/default");
			transaksi.setCreatedBy(perjalanan.getIdTraveler().getIdUser().getUsername());
			transaksi.setCreatedOn(new Date());
			transaksi.setIdGuide(perjalanan.getIdGuide().getIdUserDetail());
			transaksi.setIdTraveler(perjalanan.getIdTraveler().getIdUserDetail());
			transaksi.setIdPerjalanan(perjalanan.getIdPerjalanan());
			transaksi.setStatusPembayaran(Constanta.STATUS_BARU);
			
			DateTime tanggalBerangkat = new DateTime(perjalanan.getTanggalPergi());
			DateTime tanggalSelesai = new DateTime(perjalanan.getTanggalSelesai());
			
			Days days = Days.daysBetween(tanggalBerangkat, tanggalSelesai);
			String jumlahHari = String.valueOf(days.getDays());
			BigDecimal nominalTransaksi = perjalanan.getIdGuide().getTarifDitampilkan().multiply(new BigDecimal(jumlahHari));
			
			transaksi.setLamaPerjalanan(days.getDays());
			transaksi.setJumlahNominalTransaksi(nominalTransaksi);
			
			Transaksi trx = transaksiRepo.save(transaksi);
			
			notifTimelinePembayaran = new NotifikasiTimeline();
			notifTimelinePembayaran.setCreatedOn(new Date());
			notifTimelinePembayaran.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_PEMBAYARAN));
			notifTimelinePembayaran.setIdPerjalanan(perjalanan.getIdPerjalanan());
			notifTimelinePembayaran.setPesan(Constanta.NOTIFIKASI_PESAN_PEMBAYARAN);
			notifTimelinePembayaran.setRolePenerima(perjalanan.getIdTraveler().getIdUser().getUserRoles());
			notifTimelinePembayaran.setRolePengirim("SYSTEM");
			
			logger.debug("[CORE SERVICE] Menyimpan timeline notifikasi pembayaran");
			NotifikasiTimeline notifTl = notifikasiTimelineRepo.save(notifTimelinePembayaran);
			
			logger.debug("Trx id: "+trx.getIdTransaksi()+" notif timeline: "+notifTl.getIdNotifikasi());
			
			return new StatusResponse("success","Berhasil melakukan request perjalanan");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[CORE SERVICE] savePerjalanan() dengan id: "+perjalanan.getIdPerjalanan()+" msg: "+e.getMessage());
			return new StatusResponse("failed", "Gagal melakukan request perjalanan", e.getMessage());
		}
	}

	@Override
	public StatusResponse<Perjalanan> getPerjalananForReview(int idGuide, int idTraveler, String statusPerjalanan) {
			Perjalanan perjalanan = null;
		try {
			perjalanan = perjalananRepo.findPerjalananForReview(idGuide, idTraveler, statusPerjalanan, "BELUM");
			return new StatusResponse("success", "Berhasil mendapatkan data perjalanan", perjalanan);
		} catch (Exception e) {
			logger.error("[CORE SERVICE] getPerjalananForReview() "+e.getMessage());
			e.printStackTrace();
			return new StatusResponse("failed", "Gagal mendapatkan data perjalanan",e.getMessage());
		}
	}
	
	@Override
	public StatusResponse<List<Perjalanan>> getPerjalananByIdGuide(int idGuide) {
			List<Perjalanan> listPerjalanan = null;
		try {
			listPerjalanan = perjalananRepo.findByIdGuide(idGuide);
			return new StatusResponse("success", "Berhasil mendapatkan data perjalanan untuk id: "+idGuide, listPerjalanan);
		} catch (Exception e) {
			logger.error("[CORE SERVICE] getPerjalananByIdGuide() "+e.getMessage());
			e.printStackTrace();
			return new StatusResponse("failed", "Gagal mendapatkan data perjalanan untuk id: "+idGuide,e.getMessage());
		}
	}

	@Override
	@Transactional(readOnly=false)
	public StatusResponse updateStatusPerjalanan(int idPerjalanan, String statusPerjalanan, String pesan) {
		logger.debug("##### [SERVICE] Mengubah status perjalanan dengan id: "+idPerjalanan+" menjadi: "+statusPerjalanan);
		
		Notifikasi notifikasi = new Notifikasi();
		notifikasi.setCreatedOn(new Date());
		
		NotifikasiTipe notifTipe = new NotifikasiTipe();
		
		Notifikasi notifikasiReview = null;
		NotifikasiTipe notifTipeReview = null;
		
		Notifikasi notifSelesaiTraveler = null;
		Notifikasi notifDitolakForGuide = null;
		Notifikasi notifDibatalkanForTraveler = null;
		
		String idNotifikasi = null;
		String pesanNotifikasi = null;
		Perjalanan perjalanan = null;
		
		try {
			logger.debug("##### [SERVICE] Meminta data perjalanan dengan id: "+idPerjalanan);
			perjalanan = perjalananRepo.findByIdPerjalanan(idPerjalanan);
		} catch (Exception e) {
			logger.error("[CORE SERVICE] updateStatusPerjalanan() "+e.getMessage());
			return new StatusResponse<>("failed", "Gagal mengubah status perjalanan dengan id: "+idPerjalanan+" menjadi: "+statusPerjalanan, e.getMessage());
		}
		
		switch(statusPerjalanan){
			case Constanta.STATUS_BERJALAN:
				idNotifikasi = Constanta.NOTIFIKASI_REQ_PERJALANAN_DITERIMA;
				pesanNotifikasi = Constanta.NOTIFIKASI_PESAN_PERJALANAN_DITERIMA;
				notifikasi.setIdPengirim(perjalanan.getIdGuide());
				notifikasi.setIdPenerima(perjalanan.getIdTraveler());
				
				break;
			case Constanta.STATUS_DIBATALKAN:
				idNotifikasi = Constanta.NOTIFIKASI_PERJALANAN_DIBATALKAN;
				pesanNotifikasi = Constanta.NOTIFIKASI_PESAN_PERJALANAN_DIBATALKAN;
				notifikasi.setIdPengirim(perjalanan.getIdTraveler());
				notifikasi.setIdPenerima(perjalanan.getIdGuide());
				
				notifDibatalkanForTraveler = new Notifikasi();
				notifDibatalkanForTraveler.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_PERJALANAN_DIBATALKAN));
				notifDibatalkanForTraveler.setIdPengirim(perjalanan.getIdTraveler());
				notifDibatalkanForTraveler.setIdPenerima(perjalanan.getIdTraveler());
				notifDibatalkanForTraveler.setIdPerjalanan(perjalanan);
				notifDibatalkanForTraveler.setPesan(Constanta.NOTIFIKASI_PESAN_PERJALANAN_DIBATALKAN);
				notifDibatalkanForTraveler.setCreatedOn(new Date());
				
				break;
			case Constanta.STATUS_DITOLAK:
				idNotifikasi = Constanta.NOTIFIKASI_REQ_PERJALANAN_DITOLAK;
				pesanNotifikasi = Constanta.NOTIFIKASI_PESAN_PERJALANAN_DITOLAK;
				notifikasi.setIdPengirim(perjalanan.getIdGuide());
				notifikasi.setIdPenerima(perjalanan.getIdTraveler());
				
				notifDitolakForGuide = new Notifikasi();
				notifDitolakForGuide.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_REQ_PERJALANAN_DITOLAK));
				notifDitolakForGuide.setIdPengirim(perjalanan.getIdGuide());
				notifDitolakForGuide.setIdPenerima(perjalanan.getIdGuide());
				notifDitolakForGuide.setIdPerjalanan(perjalanan);
				notifDitolakForGuide.setPesan(Constanta.NOTIFIKASI_PESAN_PERJALANAN_DITOLAK);
				notifDitolakForGuide.setCreatedOn(new Date());
				break;
			case Constanta.STATUS_SELESAI:
				idNotifikasi = Constanta.NOTIFIKASI_PERJALANAN_SELESAI;
				pesanNotifikasi = Constanta.NOTIFIKASI_PESAN_PERJALANAN_SELESAI;
				notifikasi.setIdPengirim(perjalanan.getIdTraveler());
				notifikasi.setIdPenerima(perjalanan.getIdGuide());
				
				notifikasiReview = new Notifikasi();
				notifTipeReview = new NotifikasiTipe(Constanta.NOTIFIKASI_MEMBUAT_REVIEW);
				notifikasiReview.setIdNotifikasiTipe(notifTipeReview);
				notifikasiReview.setIdPengirim(perjalanan.getIdGuide());
				notifikasiReview.setIdPenerima(perjalanan.getIdTraveler());
				notifikasiReview.setIdPerjalanan(perjalanan);
				notifikasiReview.setPesan(Constanta.NOTIFIKASI_PESAN_BUAT_REVIEW);
				notifikasiReview.setCreatedOn(new Date());
				
				notifSelesaiTraveler = new Notifikasi();
				notifSelesaiTraveler.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_PERJALANAN_SELESAI));
				notifSelesaiTraveler.setIdPengirim(perjalanan.getIdGuide());
				notifSelesaiTraveler.setIdPenerima(perjalanan.getIdTraveler());
				notifSelesaiTraveler.setIdPerjalanan(perjalanan);
				notifSelesaiTraveler.setPesan(Constanta.NOTIFIKASI_PESAN_PERJALANAN_SELESAI);
				notifSelesaiTraveler.setCreatedOn(new Date());
				
				break;
			default:
				try {
					throw new Exception("Status perjalanan "+statusPerjalanan+" tidak ditemukan");
				} catch (Exception e1) {
					e1.printStackTrace();
					logger.error("Gagal mengubah status perjalanan dengan id: "+idPerjalanan+" menjadi: "+statusPerjalanan);
					return new StatusResponse<>("failed", "Gagal mengubah status perjalanan dengan id: "+idPerjalanan+" menjadi: "+statusPerjalanan, e1.getMessage());
				}
		}
		
		
		// Kalo ada pesan dari depan, pesannya di-override 
		if(null != pesan) pesanNotifikasi = pesan;
		
		notifTipe.setIdNotifikasiTipe(idNotifikasi);
		perjalanan.setIdPerjalanan(idPerjalanan);
		notifikasi.setIdNotifikasiTipe(notifTipe);
		notifikasi.setIdPerjalanan(perjalanan);
		notifikasi.setPesan(pesanNotifikasi);
		
		
		/*
		 * Simpan notifikasi untuk timeline status di masing2 perjalanan yang ada di dashboard.
		 * Masing-masing event menggunakan satu status saja
		 */
		
		NotifikasiTimeline notifTimeline = new NotifikasiTimeline();
		notifTimeline.setCreatedOn(new Date());
		notifTimeline.setIdNotifikasiTipe(notifTipe);
		notifTimeline.setIdPerjalanan(perjalanan.getIdPerjalanan());
		notifTimeline.setPesan(pesan);
		notifTimeline.setRolePenerima(notifikasi.getIdPenerima().getIdUser().getUserRoles());
		notifTimeline.setRolePengirim(notifikasi.getIdPengirim().getIdUser().getUserRoles());
		
		try {
			logger.debug("##### [SERVICE] Proses update status perjalanan menjadi: "+statusPerjalanan);
			
			if(idNotifikasi.equalsIgnoreCase(Constanta.NOTIFIKASI_PERJALANAN_SELESAI)){
				notifikasiRepo.save(notifSelesaiTraveler);
				notifikasiRepo.save(notifikasiReview);
			}else if(idNotifikasi.equalsIgnoreCase(Constanta.NOTIFIKASI_REQ_PERJALANAN_DITOLAK)){
				notifikasiRepo.save(notifDitolakForGuide);
			}else if(idNotifikasi.equalsIgnoreCase(Constanta.NOTIFIKASI_PERJALANAN_DIBATALKAN)){
				notifikasiRepo.save(notifDibatalkanForTraveler);
			}
			
			perjalananRepo.updateStatusPerjalanan(idPerjalanan, statusPerjalanan);
			notifikasiRepo.save(notifikasi);
			notifikasiTimelineRepo.save(notifTimeline);
			
		} catch (Exception e) {
			logger.error("[CORE SERVICE] updateStatusPerjalanan() "+e.getMessage());
			return new StatusResponse<>("failed", "Gagal mengubah status perjalanan dengan id: "+idPerjalanan+" menjadi: "+statusPerjalanan, e.getMessage());
		}
		
		return new StatusResponse<>("success", "Berhasil mengubah status perjalanan dengan id: "+idPerjalanan+" menjadi: "+statusPerjalanan);
	}

	@Override
	public StatusResponse getPerjalananByIdGuidePage(int idGuide, Pageable pageable) {
		Page<Perjalanan> pagePerjalanan = null;
		
		try {
			pagePerjalanan = perjalananRepo.findByIdGuide(idGuide, pageable);
		} catch (Exception e) {
			logger.error("[CORE SERVICE] updateStatusPerjalanan() "+e.getMessage());
			return new StatusResponse<>("failed", "Gagal mendapatkan data perjalanan berdasarkan id guide", e.getMessage());
		}
		return new StatusResponse<Page<Perjalanan>>("success", "Berhasil meminta data perjalanan berdasarkan id guide", pagePerjalanan);
	}

	@Override
	public StatusResponse getPerjalananByIdTravelerPage(int idTraveler, Pageable pageable) {
		Page<Perjalanan> pagePerjalanan = null;
		
		try {
			pagePerjalanan = perjalananRepo.findByIdTrveler(idTraveler, pageable);
		} catch (Exception e) {
			logger.error("[CORE SERVICE] updateStatusPerjalanan() "+e.getMessage());
			return new StatusResponse<>("failed", "Gagal mendapatkan data perjalanan berdasarkan id traveler", e.getMessage());
		}
		return new StatusResponse<Page<Perjalanan>>("success", "Berhasil meminta data perjalanan berdasarkan id traveler", pagePerjalanan);
	}

	@Override
	public StatusResponse getPerjalananByIdPerjalanan(int idPerjalanan) {
		Perjalanan perjalanan = null;
		
		try {
			perjalanan = perjalananRepo.findOne(idPerjalanan);
		} catch (Exception e) {
			e.printStackTrace();
			return new StatusResponse<>("gagal", "Gagal eminta data perjalanan id: "+idPerjalanan, e.getMessage());
		}
		
		return new StatusResponse<>("success", "Meminta data perjalanan id: "+idPerjalanan, perjalanan);
	}

	@Override
	@Transactional(readOnly=false)
	public StatusResponse test() {
		
		Perjalanan perjalanan = perjalananRepo.findByIdPerjalanan(2);
		
		Transaksi transaksi = new Transaksi();
		transaksi.setCreatedBy("testcase");
		transaksi.setCreatedOn(new Date());
		transaksi.setIdGuide(perjalanan.getIdGuide().getIdUserDetail());
		transaksi.setIdTraveler(perjalanan.getIdTraveler().getIdUserDetail());
		transaksi.setIdPerjalanan(perjalanan.getIdPerjalanan());
		transaksi.setStatusPembayaran(Constanta.STATUS_BARU);
		
		DateTime tanggalBerangkat = new DateTime(perjalanan.getTanggalPergi());
		DateTime tanggalSelesai = new DateTime(perjalanan.getTanggalSelesai());
		
		Days days = Days.daysBetween(tanggalBerangkat, tanggalSelesai);
		String jumlahHari = String.valueOf(days.getDays());
		BigDecimal nominalTransaksi = perjalanan.getIdGuide().getTarif().multiply(new BigDecimal(jumlahHari));
		
		transaksi.setJumlahNominalTransaksi(nominalTransaksi);
		Transaksi trx = transaksiRepo.save(transaksi);
		
		
		
		
		NotifikasiTimeline notifTimelinePembayaran = new NotifikasiTimeline();
		notifTimelinePembayaran.setCreatedOn(new Date());
		notifTimelinePembayaran.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_PEMBAYARAN));
		notifTimelinePembayaran.setIdPerjalanan(perjalanan.getIdPerjalanan());
		notifTimelinePembayaran.setPesan(Constanta.NOTIFIKASI_PESAN_PEMBAYARAN);
		notifTimelinePembayaran.setRolePenerima(perjalanan.getIdGuide().getIdUser().getUserRoles());
		notifTimelinePembayaran.setRolePengirim(perjalanan.getIdTraveler().getIdUser().getUserRoles());
		
		logger.debug("[CORE SERVICE] Menyimpan timeline notifikasi pembayaran");
		NotifikasiTimeline notifTl = notifikasiTimelineRepo.save(notifTimelinePembayaran);
		
		Map<String, Object> testResult = new HashMap<>();
		testResult.put("transaksi", trx);
		testResult.put("notif", notifTimelinePembayaran);
		
		return new StatusResponse<>("success", "success save", testResult);
	}

}
