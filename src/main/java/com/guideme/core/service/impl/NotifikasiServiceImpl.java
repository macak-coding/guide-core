package com.guideme.core.service.impl;

import java.util.List;

import javax.management.Notification;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guideme.common.entity.Notifikasi;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.repository.NotifikasiRepository;
import com.guideme.core.service.inf.NotifikasiServiceInf;

@Service
@Transactional(readOnly=true)
public class NotifikasiServiceImpl implements NotifikasiServiceInf{
	Logger logger = Logger.getLogger(NotifikasiServiceImpl.class);
	
	NotifikasiRepository notifikasiRepo;
	
	@Autowired
	public NotifikasiServiceImpl(NotifikasiRepository notifikasiRepo) {
		this.notifikasiRepo = notifikasiRepo;
	}
	
	@Override
	public StatusResponse getNotifikasiByIdPenerima(int idPenerima) {
		logger.debug("####### [SERVICE] Meminta list notifikasi dengan id penerima: "+idPenerima);
		
		List listNotifikasi = null;
		
		try {
			listNotifikasi = notifikasiRepo.findByIdPenerima(idPenerima);
		} catch (Exception e) {
			logger.error("[CORE SERVICE] getNotifikasiByIdPenerima() "+e.getMessage());
			return new StatusResponse("failed", "Gagal meminta list notifikasi berdasarkan id penerima", e.getMessage());
		}
		return new StatusResponse<List<Notifikasi>>("success", "Berhasil meminta list notifikasi berdasarkan id penerima", listNotifikasi);
	}
	
	@Override
	public StatusResponse getNotifikasiByIdPengirim(int idPengirim) {
		logger.debug("####### [SERVICE] Meminta list notifikasi dengan id pengirim: "+idPengirim);
		
		List listNotifikasi = null;
		
		try {
			listNotifikasi = notifikasiRepo.findByIdPengirim(idPengirim);
		} catch (Exception e) {
			logger.error("[CORE SERVICE] getNotifikasiByIdPengirim() "+e.getMessage());
			return new StatusResponse("failed", "Gagal meminta list notifikasi berdasarkan id pengirim", e.getMessage());
		}
		return new StatusResponse<List<Notifikasi>>("success", "Berhasil meminta list notifikasi berdasarkan id pengirim", listNotifikasi);	
	}

	@Override
	public StatusResponse getNotifikasiByIdNotifikasiAndIdPenerima(int idNotifikasi, int idPenerima) {
		logger.debug("####### [SERVICE] Meminta list notifikasi dengan id penerima: "+idPenerima+", id notifikasi: "+idNotifikasi);
		
		Notifikasi notifikasi = null;
		
		try {
			notifikasi = notifikasiRepo.findByIdNotifikasiAndIdPenerima(idNotifikasi, idPenerima);
		} catch (Exception e) {
			logger.error("[CORE SERVICE] getNotifikasiByIdNotifikasiAndIdPenerima() "+e.getMessage());
			return new StatusResponse("failed", "Gagal meminta list notifikasi berdasarkan id notifikasi", e.getMessage());
		}
		return new StatusResponse<Notifikasi>("success", "BErhasil meminta list notifikasi berdasarkan id notifikasi", notifikasi);
	}

	@Override
	public Page<Notifikasi> getNotifikasiByIdPenerimaPageable(int idPenerima, Pageable pageable) {
		logger.debug("####### [SERVICE] Meminta list notifikasi dengan id penerima pageable: "+idPenerima);
		
		Page<Notifikasi> pageNotifikasi = null;
		
		try {
			pageNotifikasi = notifikasiRepo.findByIdPenerimaPageable(idPenerima, pageable);
		} catch (Exception e) {
			logger.error("[CORE SERVICE] getNotifikasiByIdPenerimaPageable() "+e.getMessage());
		}
		return pageNotifikasi;
	}

	@Override
	public StatusResponse getNotifikasiByIdPerjalananPageable(int idPerjalanan, Pageable pageable) {
		logger.debug("####### [SERVICE] Meminta list notifikasi dengan id perjalanan: "+idPerjalanan);
		
		Page<Notifikasi> pageNotifikasi = null;
		
		try {
			pageNotifikasi = notifikasiRepo.findByIdPerjalanan(idPerjalanan, pageable);
		} catch (Exception e) {
			logger.error("[CORE SERVICE] getNotifikasiByIdNotifikasiAndIdPenerima() "+e.getMessage());
			return new StatusResponse("failed", "Gagal meminta list notifikasi berdasarkan id notifikasi", e.getMessage());
		}
		
		return new StatusResponse<Page<Notifikasi>>("success", "BErhasil meminta list notifikasi berdasarkan id notifikasi", pageNotifikasi);
	}

}
