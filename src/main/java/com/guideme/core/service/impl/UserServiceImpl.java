package com.guideme.core.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guideme.common.entity.User;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.repository.UserRepository;
import com.guideme.core.service.inf.UserServiceInf;

@Service
@Transactional(readOnly=true)
public class UserServiceImpl implements UserServiceInf{
	Logger logger = Logger.getLogger(UserServiceImpl.class);
	@Autowired
	UserRepository userRepository;
	
	@Override
	public User getUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}
	
	@Override
	public void saveUser(User user) {
		userRepository.save(user);
	}

	@Override
	@Transactional(readOnly=false)
	public void updateEmail(String email, int idUser) throws Exception {
		User user = userRepository.findByEmail(email);
		
		if(null == user){
			userRepository.updateEmail(email, idUser);
		}else{
			throw new Exception("Email already exist");
		}
	}

	@Override
	public StatusResponse checkUsername(String username) {
		User user = null;
		
		try {
			user = userRepository.findByUsername(username);;
		} catch (Exception e) {
			return new StatusResponse<>("failed", "Gagal mendapatkan data username", e.getMessage());
		}
		return new StatusResponse<User>("success", "Berhasil mendapatkan data username", user);
	}
	
	@Override
	public StatusResponse checkEmail(String email) {
		User user = null;
		
		try {
			user = userRepository.findByEmail(email);;
		} catch (Exception e) {
			return new StatusResponse<>("failed", "Gagal mendapatkan data email", e.getMessage());
		}
		return new StatusResponse<User>("success", "Berhasil mendapatkan data email", user);
	}
	
	@Override
	public StatusResponse checkPassword(String username, String password) {
		User user = null;
		try {
			user = userRepository.findByUsernameAndPassword(username, password);
		} catch (Exception e) {
			return new StatusResponse("failed", "Gagal cek user",e.getMessage());
		}
		return new StatusResponse("success", "Sukses cek user", user);
	}

	@Override
	@Transactional(readOnly=false)
	public void updatePassword(String password, int idUser) {
		try {
			userRepository.updatePassword(password, idUser);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	@Override
	@Transactional(readOnly=false)
	public StatusResponse verifyUser(int idUser) {
		try {
			userRepository.updateStatusVerifikasi("VERIFIED", idUser);
			return new StatusResponse<>("success", "Berhasil memverifikasi user");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Gagal verify user dengan id: "+idUser);
			return new StatusResponse<>("failed", "Gagal memverifikasi user", e.getMessage());
		}
	}
	
	@Override
	@Transactional(readOnly=false)
	public StatusResponse unverifyUser(int idUser) {
		try {
			userRepository.updateStatusVerifikasi("UNVERIFIED", idUser);
			return new StatusResponse<>("success", "Berhasil unverify user");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Gagal unverify user dengan id: "+idUser);
			return new StatusResponse<>("failed", "Gagal unverify user", e.getMessage());
		}
	}
	
	@Override
	@Transactional(readOnly=false)
	public StatusResponse suspendUser(int idUser) {
		try {
			userRepository.updateStatusAkun("SUSPENDED", idUser);
			return new StatusResponse<>("success", "Berhasil suspend user");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Gagal suspend user dengan id: "+idUser);
			return new StatusResponse<>("failed", "Gagal suspend user", e.getMessage());
		}
	}

	@Override
	public StatusResponse getUserByUsernameOrEmail(String username, String email) {
		User user = null;
		
		try {
			user = userRepository.findByUsernameOrEmail(username, email);
		} catch (Exception e) {
			return new StatusResponse("failed", "Gagal mendapatkan data dengen user/email: "+username+"/"+email, e.getMessage());
		}
		return new StatusResponse<User>("success", "Berhasil mendapatkan data dengan emamil/username: "+username+"/"+email, user);
	} 

}
