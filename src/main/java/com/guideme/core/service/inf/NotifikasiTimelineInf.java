package com.guideme.core.service.inf;

import java.util.List;

import com.guideme.common.entity.NotifikasiTimeline;
import com.guideme.common.model.StatusResponse;

public interface NotifikasiTimelineInf {
	StatusResponse<List<NotifikasiTimeline>> getNotifikasiTimelineByIdPerjalanan(int idPerjalanan);
	StatusResponse<NotifikasiTimeline> approvePembayaran(int idTransksi);
	StatusResponse selesaikanPembayaran(int idTransaksi);
	StatusResponse<NotifikasiTimeline>batalkanPembayaran(int idTransksi);
	StatusResponse<NotifikasiTimeline>ulangiPembayaran(int idTransksi);
}
