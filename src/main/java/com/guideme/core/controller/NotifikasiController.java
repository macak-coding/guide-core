package com.guideme.core.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.guideme.common.entity.Notifikasi;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.service.inf.NotifikasiServiceInf;

@Controller
@RequestMapping("/notifikasi")
public class NotifikasiController {
	Logger logger = Logger.getLogger(NotifikasiController.class);
	
	NotifikasiServiceInf notifikasiService = null;
	
	@Autowired
	public NotifikasiController(NotifikasiServiceInf notifikasiService) {
		this.notifikasiService = notifikasiService;
	}
	
	@RequestMapping(value={"","/"})
	public String defafult(){
		return "OK";
	}
	
	@RequestMapping("/getByIdPenerima/{idPenerima}")
	@ResponseBody
	public StatusResponse getNotifikasiByIdPenerima(@PathVariable("idPenerima") int idPenerima){
		logger.debug("####### [CONTROLLER] Meminta list notifikasi dengan id penerima: "+idPenerima);
		
		StatusResponse statusResponse = null;
		try {
			statusResponse = notifikasiService.getNotifikasiByIdPenerima(idPenerima);
		} catch (Exception e) {
			return new StatusResponse<>("failed", "####### [CONTROLLER] Meminta list notifikasi dengan id penerima: "+idPenerima, e.getMessage());
		}
		
		return statusResponse;
	}
	
	@RequestMapping("/getByIdNotifikasiAndIdPenerima/{idNotifikasi}/{idPenerima}")
	@ResponseBody
	public StatusResponse getNotifikasiByIdNotifikasiAndIdPenerima(@PathVariable("idPenerima") int idPenerima, @PathVariable("idNotifikasi") int idNotifikasi){
		logger.debug("####### [CONTROLLER] Meminta list notifikasi dengan id penerima: "+idPenerima+" dan id notifikasi: "+idNotifikasi);
		
		StatusResponse statusResponse = null;
		try {
			statusResponse = notifikasiService.getNotifikasiByIdNotifikasiAndIdPenerima(idNotifikasi, idPenerima);
		} catch (Exception e) {
			return new StatusResponse<>("failed", "####### [CONTROLLER] Meminta list notifikasi dengan id penerima: "+idPenerima+" dan id notifikasi: "+idNotifikasi, e.getMessage());
		}
		
		return statusResponse;
	}
	
	@RequestMapping("/getByIdPenerimaPageable/{idPenerima}")
	@ResponseBody
	public Page<Notifikasi> getNotifikasiByIdPenerimaPageable(@PathVariable("idPenerima") int idPenerima, Pageable pageable){
		logger.debug("####### [CONTROLLER] Meminta list notifikasi dengan id penerima pageable: "+idPenerima);
		
		Page<Notifikasi> pageNotifikasi = null;
		try {
			pageNotifikasi = notifikasiService.getNotifikasiByIdPenerimaPageable(idPenerima, pageable);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return pageNotifikasi;
	}
	
	@RequestMapping("/getByIdPerjalananPageable/{idPerjalanan}")
	@ResponseBody
	public StatusResponse getNotifikasiByIdPerjalananPageable(@PathVariable("idPerjalanan") int idPerjalanan, Pageable pageable){
		logger.debug("####### [CONTROLLER] Meminta list notifikasi dengan id perjalanan pageable: "+idPerjalanan);
		
		StatusResponse statusResponse = null;
		try {
			statusResponse = notifikasiService.getNotifikasiByIdPerjalananPageable(idPerjalanan, pageable);
		} catch (Exception e) {
			return new StatusResponse<Page<Notifikasi>>("failed", "####### [CONTROLLER] Meminta list notifikasi dengan id perjalanan pageable: "+idPerjalanan, e.getMessage());
		}
		
		return statusResponse;
	}
	
}
