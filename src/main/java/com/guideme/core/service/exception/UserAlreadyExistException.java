package com.guideme.core.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class UserAlreadyExistException extends Exception{
	public UserAlreadyExistException(String message){
		super(message);
	}
}
