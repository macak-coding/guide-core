package com.guideme.core.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guideme.common.entity.EmailTemplate;
import com.guideme.common.entity.TokenVerifikasi;
import com.guideme.common.model.EmailModel;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.repository.EmailTemplateRepository;
import com.guideme.core.repository.TokenVerifikasiRepository;
import com.guideme.core.repository.UserDetailRepository;
import com.guideme.core.repository.UserRepository;
import com.guideme.core.service.inf.EmailServiceInf;
import com.guideme.core.service.inf.TokenVerifikasiInf;

@Service
@Transactional(readOnly=true)
public class TokenVrifikasiImpl implements TokenVerifikasiInf{
	Logger logger = Logger.getLogger(TokenVrifikasiImpl.class);
	
	@Value("${ohmyguide.gui.server.address}")
	String GUI_SRVER_ADDRESS;
	
	TokenVerifikasiRepository tokenRepo;
	UserRepository userRepo;
	EmailServiceInf emailService;
	EmailTemplateRepository emailTemplateRepo;
	
	@Autowired
	TokenVrifikasiImpl(TokenVerifikasiRepository tokenRepo, EmailServiceInf emailService,
					UserRepository userRepo, EmailTemplateRepository emailTemplateRepo){
		this.tokenRepo = tokenRepo;
		this.emailService = emailService;
		this.userRepo = userRepo;
		this.emailTemplateRepo = emailTemplateRepo;
	}
	
	@Override
	@Transactional(readOnly=false)
	public String createAndSaveEmailToken(String email) {
		String token = UUID.randomUUID().toString();
		TokenVerifikasi tokenVerifikasi = new TokenVerifikasi();
		tokenVerifikasi.setTokenCode(token);
		tokenVerifikasi.setStatus("UNVERIFIED");
		tokenVerifikasi.setEmail(email);
		tokenRepo.save(tokenVerifikasi);
		
		Map<String, String> param = new HashMap<>();
		param.put("email", email);
		param.put("urlVerifikasiEmail", GUI_SRVER_ADDRESS+"/register/verify-email");
		param.put("token", token);
		
		EmailTemplate emailTemplate = emailTemplateRepo.findByKodeEmailTemplate("REG001");
		
		EmailModel emailModel = new EmailModel();
		emailModel.setTo(email);
		emailModel.setSubject(emailTemplate.getSubject());
		emailModel.setTemplateName(emailTemplate.getNamaTemplate());
		
		
		emailService.sendHtmlEmail(emailModel, param);
		
		return token;
	}
	
	@Override
	@Transactional(readOnly=false)
	public Map<String, String> verifyEmail(String token){
		TokenVerifikasi tokenVerifikasi = tokenRepo.findByTokenCodeAndStatus(token, "UNVERIFIED");
		Map<String, String> result = new HashMap<>();
		
		if(null == tokenVerifikasi){
			result.put("status", "failed");
			return result;
		}
		else{
			Map<String, String> param = new HashMap<>();
			param.put("email", tokenVerifikasi.getEmail());
			param.put("urlLogin", GUI_SRVER_ADDRESS);
			
			EmailTemplate emailTemplate = emailTemplateRepo.findByKodeEmailTemplate("REG002");
			
			EmailModel emailModel = new EmailModel();
			emailModel.setTo(tokenVerifikasi.getEmail());
			emailModel.setSubject(emailTemplate.getSubject());
			emailModel.setTemplateName(emailTemplate.getNamaTemplate());
			emailService.sendHtmlEmail(emailModel, param);
			
			tokenRepo.updateStatusByToken(token, "VERIFIED");
			userRepo.updateStatusAkunByEmail("ACTIVE",tokenVerifikasi.getEmail());
			
			result.put("status", "success");
			result.put("email", tokenVerifikasi.getEmail());
			
			return result;
		}
	}

}
