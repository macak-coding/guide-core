package com.guideme.core.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guideme.common.entity.ChatMessage;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.repository.ChatMessageRepository;
import com.guideme.core.service.inf.ChatMessageServiceInf;

@Service
@Transactional(readOnly=true)
public class ChatMessageServiceImpl implements ChatMessageServiceInf{
	private static final Logger logger = Logger.getLogger(ChatMessageServiceImpl.class);
	
	private ChatMessageRepository chatMessageRepo;
	
	@Autowired
	public ChatMessageServiceImpl(ChatMessageRepository chatMessageRepo){
		this.chatMessageRepo = chatMessageRepo;
	}
	
	
	@Override
	public StatusResponse getByTokenPageable(String token, Pageable pageable) {
		StatusResponse statusResp = null;
		Page<ChatMessage> chatDataPage = null;
		
		try {
			chatDataPage = chatMessageRepo.findByChatToken(token, pageable);
		} catch (Exception e) {
			logger.error("[CORE SRVICE] Gagal get chat by token: "+token+", msg: "+e.getMessage());
			e.printStackTrace();
			return new StatusResponse<>("failed", "[CORE SRVICE] Gagal get chat by token: "+token, e.getMessage());
		}
		
		return new StatusResponse("success", "Berhasil mendapat list chat dengan token: "+token, chatDataPage);
	}


	@Override
	@Transactional(readOnly=false)
	public StatusResponse saveChatMessage(ChatMessage chatMessage) {
		
		try {
			chatMessageRepo.save(chatMessage);
		} catch (Exception e) {
			logger.error("[SERVICE CORE] Save chat message gagal, token: "+chatMessage.getChatToken()+", pengirim: "+chatMessage.getPengirim()+", penerima: "+chatMessage.getPenerima());
			e.printStackTrace();
			new StatusResponse<>("failed", "[SERVICE CORE] Gagal menyimpan chat message", e.getMessage());
		}
		return new StatusResponse<>("success", "[SERVICE CORE] Berhasil menyimpan chat message");
	}


	@Override
	public StatusResponse getByPengirim(String pengirim, Pageable pageable) {
		StatusResponse statusResp = null;
		Page<ChatMessage> chatDataPage = null;
		
		try {
			chatDataPage = chatMessageRepo.findByPengirim(pengirim, pageable);
		} catch (Exception e) {
			logger.error("[CORE SRVICE] Gagal get chat by pengirim: "+pengirim+", msg: "+e.getMessage());
			e.printStackTrace();
			return new StatusResponse<>("failed", "[CORE SRVICE] Gagal get chat by pengirim: "+pengirim, e.getMessage());
		}
		
		return new StatusResponse("success", "Berhasil mendapat list chat dengan pengirim: "+pengirim, chatDataPage);
	}


	@Override
	public StatusResponse getByPenerima(String penerima, Pageable pageable) {
		StatusResponse statusResp = null;
		Page<ChatMessage> chatDataPage = null;
		
		try {
			chatDataPage = chatMessageRepo.findByPenerima(penerima, pageable);
		} catch (Exception e) {
			logger.error("[CORE SRVICE] Gagal get chat by penerima: "+penerima+", msg: "+e.getMessage());
			e.printStackTrace();
			return new StatusResponse<>("failed", "[CORE SRVICE] Gagal get chat by penerima: "+penerima, e.getMessage());
		}
		
		return new StatusResponse("success", "Berhasil mendapat list chat dengan penerima: "+penerima, chatDataPage);
	}

}
