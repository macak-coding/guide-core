package com.guideme.core.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.guideme.common.entity.JalanBareng;

@Repository
public interface JalanBarengRepository extends PagingAndSortingRepository<JalanBareng, Integer>{
	public JalanBareng findByTokenId(String tokenId);
	
	@Modifying(clearAutomatically = true)
	@Query("update JalanBareng jlnBrg set jlnBrg.statusPerjalanan = :statusPerjalanan where jlnBrg.idJalanBareng = :idJalanBareng")
	public void updateStatusJalanBareng(@Param("idJalanBareng") int idJalanBareng, @Param("statusPerjalanan") String statusPerjalanan);
}
