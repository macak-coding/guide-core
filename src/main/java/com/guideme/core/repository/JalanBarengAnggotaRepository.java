package com.guideme.core.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.guideme.common.entity.JalanBarengAnggota;
import com.guideme.common.entity.UserDetail;

@Repository
public interface JalanBarengAnggotaRepository extends PagingAndSortingRepository<JalanBarengAnggota, Integer>{
	public JalanBarengAnggota findByIdAnggota(UserDetail idAnggota);
	
	@Modifying(clearAutomatically = true)
	@Query("update JalanBarengAnggota anggota set anggota.status = :status where anggota.idAnggota.idUserDetail = :idAnggota and anggota.jalanBareng.idJalanBareng = :idJalanBareng")
	public void updateStatusAnggota(@Param("idAnggota") int idAnggota, @Param("idJalanBareng") int idJalanBareng, @Param("status") String status);
	
	@Query("SELECT angg FROM JalanBarengAnggota angg"
			+ " JOIN FETCH angg.idAnggota idAngg"
			+ " JOIN FETCH idAngg.idUser idUsr"
			+ " WHERE idUsr.username = :username"
			+ " AND angg.jalanBareng.tokenId = :token")
	public JalanBarengAnggota findByUsername(@Param("username") String username, @Param("token") String token);
}
