package com.guideme.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.guideme.common.entity.Kota;
import com.guideme.common.entity.Notifikasi;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.entity.ViewDtTransaksi;
import com.guideme.common.entity.ViewDtUserDetail;

@Repository
public interface ViewDtTransaksiRepository extends PagingAndSortingRepository<ViewDtTransaksi, Integer>{
}
