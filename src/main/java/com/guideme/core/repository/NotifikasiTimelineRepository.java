package com.guideme.core.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.guideme.common.entity.NotifikasiTimeline;

@Repository
public interface NotifikasiTimelineRepository extends PagingAndSortingRepository<NotifikasiTimeline, Integer>{
	List<NotifikasiTimeline> findByIdPerjalananOrderByIdNotifikasiDesc(Integer idPerjalanan);
}
