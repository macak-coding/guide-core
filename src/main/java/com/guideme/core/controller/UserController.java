package com.guideme.core.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.guideme.common.entity.User;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.service.inf.UserDetailServiceInf;
import com.guideme.core.service.inf.UserServiceInf;

@RestController
@RequestMapping("/user")
public class UserController {
	private static Logger logger = Logger.getLogger(UserController.class);

	@Autowired
	UserServiceInf userService;
	
	@Autowired
	UserDetailServiceInf userDetailService;
	
	@RequestMapping(value={"","/"},method = RequestMethod.GET)
	public String status() {
		return "OK";
	}
	
	@RequestMapping(value="/{username:.+}",method = RequestMethod.GET)
	public User getUserByUsername(@PathVariable String username) {
		logger.debug("Meminta informasi username: "+username);
		User authUser = userService.getUserByUsername(username);
		return authUser;
	}
	
	@RequestMapping(value="/usernameOrEmail/{username}/{email}",method = RequestMethod.GET)
	public StatusResponse usernameOrEmail(@PathVariable("username") String username, @PathVariable("email") String email) {
		logger.debug("Meminta informasi username/email: "+username+"/"+email);
		StatusResponse authResp = userService.getUserByUsernameOrEmail(username, email);
		return authResp;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity createUser(@RequestBody User user) {
		if(null != user){
			userService.saveUser(user);
			return ResponseEntity.ok().build();
		}else{
			return ResponseEntity.badRequest().build();
		}
	}
	
	@RequestMapping(value="/update-email",method = RequestMethod.POST)
	public ResponseEntity updateEmail(@RequestBody User user) {
		if(null != user){
			try {
				userService.updateEmail(user.getEmail(), user.getIdUser());
			} catch (Exception e) {
				logger.info("Update email "+user.getEmail()+" telah terdaftar.");
				return ResponseEntity.ok(new StatusResponse("Failed", "Email telah terdaftar!"));
			}
			
			logger.info("Update email "+user.getEmail()+" telah berhasil diupdate.");
			return ResponseEntity.ok(new StatusResponse("Success", "Email berhasil diupdate!"));
		}else{
			logger.info("Update email "+user.getEmail()+" gagal diupdate, data yang dikirim tidak valid.");
			return ResponseEntity.badRequest().body(new StatusResponse("Failed", "Data yang dikirim tidak valid."));
		}
	}
	
	@RequestMapping(value="/update-password",method = RequestMethod.PUT)
	public ResponseEntity updatePassword(@RequestBody User user) {
		if(null != user){
				
			userService.updatePassword(user.getPassword(), user.getIdUser());
			
			logger.debug("Update password "+user.getIdUser()+" berhasil.");
			return ResponseEntity.ok(new StatusResponse("success", "Password berhasil diupdate!"));
		}else{
			logger.info("Password user "+user.getIdUser()+" gagal diupdate, data yang dikirim tidak valid.");
			return ResponseEntity.badRequest().body(new StatusResponse("failed", "Data yang dikirim tidak valid."));
		}
	}
	
	@RequestMapping(value="/detail/{username}",method = RequestMethod.GET)
	public UserDetail getUserDetailByUsername(@PathVariable String username) {
		UserDetail userDetail = (UserDetail) userDetailService.getUserDetailByUsername(username).getReturnObject();
		return userDetail;
	}
	
	@RequestMapping(value="/checkPassword/{username}/{password}",method = RequestMethod.GET)
	public ResponseEntity checkPassword(@PathVariable String username, @PathVariable String password) {
		logger.debug("Checking username|password "+username+"|"+password);
		
		StatusResponse resp = userService.checkPassword(username,password);
		return ResponseEntity.ok(resp);
	}
	
	@RequestMapping(value="/checkUsernameExist",method = RequestMethod.POST)
	public ResponseEntity checkUsernameExist(@RequestBody User user) {
		StatusResponse<User> statusResponse = null;
		try {
			statusResponse = userService.checkUsername(user.getUsername());
		} catch (Exception e) {
			return ResponseEntity.ok(new StatusResponse("failed", "[CONTROLLER] Gagal check username", e.getMessage()));
		}
		
		return ResponseEntity.ok(statusResponse);
	}
	
	@RequestMapping(value="/checkEmailExist",method = RequestMethod.POST)
	public ResponseEntity checkEmailExist(@RequestBody User user) {
		StatusResponse<User> statusResponse = null;
		try {
			statusResponse = userService.checkEmail(user.getEmail());
		} catch (Exception e) {
			return ResponseEntity.ok(new StatusResponse("failed", "[CONTROLLER] Gagal check email", e.getMessage()));
		}
		
		return ResponseEntity.ok(statusResponse);
	}
	
	@RequestMapping(value="/verify/{idUser}",method = RequestMethod.PUT)
	public ResponseEntity verifyUser(@PathVariable("idUser") int idUser) {
		StatusResponse statusResponse = null;
		statusResponse = userService.verifyUser(idUser);
		
		return ResponseEntity.ok(statusResponse);
	}
	
	@RequestMapping(value="/unverify/{idUser}",method = RequestMethod.PUT)
	public ResponseEntity unverifyUser(@PathVariable("idUser") int idUser) {
		StatusResponse statusResponse = null;
		statusResponse = userService.unverifyUser(idUser);
		
		return ResponseEntity.ok(statusResponse);
	}
	
	@RequestMapping(value="/suspend/{idUser}",method = RequestMethod.PUT)
	public ResponseEntity suspendUser(@PathVariable("idUser") int idUser) {
		StatusResponse statusResponse = null;
		statusResponse = userService.suspendUser(idUser);
		
		return ResponseEntity.ok(statusResponse);
	}
}
