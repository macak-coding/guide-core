package com.guideme.core.service.inf;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.guideme.common.entity.Perjalanan;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.StatusResponse;

public interface PerjalananServiceInf {
	public StatusResponse createPerjalanan(Perjalanan perjalanan);
	public StatusResponse getPerjalananForReview(int idGuide, int idTraveler, String statusPerjalanan);
	StatusResponse<List<Perjalanan>> getPerjalananByIdGuide(int idGuide);
	public StatusResponse updateStatusPerjalanan(int idPerjalanan, String statusPerjalanan, String pesan);
	public StatusResponse getPerjalananByIdGuidePage(int idGuide, Pageable pageable);
	public StatusResponse getPerjalananByIdTravelerPage(int idTraveler, Pageable pageable);
	public StatusResponse getPerjalananByIdPerjalanan(int idPerjalanan);
	public StatusResponse test();
}
