package com.guideme.core.service.inf;

import java.util.List;

import javax.management.Notification;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.guideme.common.entity.Notifikasi;
import com.guideme.common.model.StatusResponse;

public interface NotifikasiServiceInf {
	StatusResponse getNotifikasiByIdPenerima(int idPenerima);
	StatusResponse<Page<Notifikasi>> getNotifikasiByIdPerjalananPageable(int idPerjalanan, Pageable pageable);
	Page<Notifikasi> getNotifikasiByIdPenerimaPageable(int idPenerima, Pageable pageable);
	StatusResponse getNotifikasiByIdPengirim(int idPengirim);
	StatusResponse getNotifikasiByIdNotifikasiAndIdPenerima(int idNotifikasi, int idPenerima);
}
