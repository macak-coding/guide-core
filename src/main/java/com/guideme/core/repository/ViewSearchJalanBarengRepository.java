package com.guideme.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.guideme.common.entity.ViewSearchJalanBareng;

@Repository
public interface ViewSearchJalanBarengRepository extends PagingAndSortingRepository<ViewSearchJalanBareng, String>{
	public Page<ViewSearchJalanBareng> findByKotaTujuanContainingIgnoreCase(String kotaTujuan, Pageable pageable);

}
