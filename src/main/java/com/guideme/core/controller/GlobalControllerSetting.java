package com.guideme.core.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.guideme.common.model.StatusResponse;
import com.guideme.core.service.exception.EmailAlreadyExistException;
import com.guideme.core.service.exception.UserAlreadyExistException;

import org.springframework.http.HttpStatus;

@ControllerAdvice  
@RestController 
public class GlobalControllerSetting {
	
	/*@ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(value = UserAlreadyExistException.class)  
    public StatusResponse userAlreadyExist(){  
        return new StatusResponse("Failed","User already registered");  
    }
	
	@ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(value = EmailAlreadyExistException.class)  
    public StatusResponse emailAlreadyExist(){  
        return new StatusResponse("Failed","Email already registered");  
    } */ 
}
