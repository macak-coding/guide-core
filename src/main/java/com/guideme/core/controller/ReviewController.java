package com.guideme.core.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.guideme.common.entity.RatingPerjalanan;
import com.guideme.common.entity.Review;
import com.guideme.common.model.ReviewWrapper;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.service.inf.ReviewServiceInf;

@Controller
@RestController
@RequestMapping("/review")
public class ReviewController {
	Logger logger = Logger.getLogger(ReviewController.class);
	
	@Autowired ReviewServiceInf reviewInf;
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<StatusResponse> saveReview(@RequestBody ReviewWrapper reviewWrapper){
		Review review = reviewWrapper.getReview();
		RatingPerjalanan ratingPerjalanan = reviewWrapper.getRatingPerjalanan();
		
		try {
			StatusResponse statusResponse = reviewInf.saveReviewGuide(review, ratingPerjalanan);
			return ResponseEntity.ok(statusResponse);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return ResponseEntity.ok(new StatusResponse("failed", "Exception di service ceore", e.getMessage()));
			
		}
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<StatusResponse> getAllReview(){
		
		try {
			StatusResponse statusResponse = reviewInf.getAllReview();
			return ResponseEntity.ok(statusResponse);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return ResponseEntity.ok(new StatusResponse("failed", "Exception di service ceore", e.getMessage()));
			
		}
	}
}
