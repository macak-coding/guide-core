package com.guideme.core.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.guideme.common.entity.Transaksi;
import com.guideme.common.entity.ViewDtTransaksi;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.service.inf.TransaksiServiceInf;

@RestController
@RequestMapping("/transaksi")
public class TransaksiController {
	Logger logger = Logger.getLogger(TransaksiController.class);
	
	private TransaksiServiceInf transaksiService;
	
	@Autowired
	public TransaksiController(TransaksiServiceInf transaksiService){
		this.transaksiService = transaksiService;
	}
	
	@RequestMapping(value="/buktiPembayaran", method = RequestMethod.PUT)
	public StatusResponse updateBuktiPembayaran(@RequestBody Transaksi transaksi){
		return transaksiService.updateBuktiTransaksi(transaksi.getIdTransaksi(), transaksi.getStatusPembayaran(), transaksi.getUrlBuktiPembayaran());
	}
	
	@RequestMapping(value="/{kodeTransaksi}/{username}", method = RequestMethod.GET)
	public StatusResponse getbyIdTransaksi(@PathVariable("kodeTransaksi") String kodeTransaksi, @PathVariable("username") String username){
		return transaksiService.getByIdTransaksi(kodeTransaksi, username);
	}
	
	@RequestMapping(value="/idPerjalanan/{idPerjalanan}", method = RequestMethod.GET)
	public StatusResponse<Transaksi> getbyIdPerjalanan(@PathVariable("idPerjalanan") int idPerjalanan){
		return transaksiService.getByIdPerjalanan(idPerjalanan);
	}
	
	@RequestMapping(value="/getForAdmin", method = RequestMethod.GET)
	public StatusResponse<Iterable<ViewDtTransaksi>> getAllForAdmin(){
		return transaksiService.getListTransaksiForAdmin();
	}
}
