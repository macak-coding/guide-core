package com.guideme.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guideme.common.entity.Kota;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.repository.KotaRepository;
import com.guideme.core.service.inf.KotaServiceInf;

@Service
@Transactional(readOnly=true)
public class KotaServiceImpl implements KotaServiceInf{
	
	@Autowired KotaRepository kotaRepository;
	
	@Override
	public List<Kota> getAllKota() {
		return (List<Kota>) kotaRepository.findAll();
	}
	
	@Override
	public Page<Kota> getAllKotaPageable(Pageable pageable) {
		return  kotaRepository.findAll(pageable);
	}

	@Override
	public List<Kota> getKotaLike(String namaKota) {
		List<Kota> listKota = null;
		try {
			listKota =  kotaRepository.findByNamaKotaContainingIgnoreCase(namaKota);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listKota;
	}

	@Override
	@Transactional(readOnly=false)
	public StatusResponse saveKota(List<Kota> listKota) {
		try {
			kotaRepository.save(listKota);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new StatusResponse("success", "Berhasil menyimpan data kota");
	}
}
