package com.guideme.core.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.guideme.common.entity.UserDetail;

@Repository
public interface UserDetailRepository extends 
				 PagingAndSortingRepository<UserDetail, Integer>, JpaSpecificationExecutor<UserDetail>{
	
	@Query(value="select ud from UserDetail ud where ud.idUser.username = :username")
	public UserDetail findByUsername(@Param("username") String username);
	
	public UserDetail findByIdUserDetail(int idUserDetail);
	
	@Modifying(clearAutomatically = true)
	@Query("update UserDetail ud set ud.statusUser =:newStatus where ud.idUserDetail =:idUserDetail")
	public void updateUserStatus(@Param("idUserDetail") int idUserDetail, @Param("newStatus") String newStatus);
	
	@Modifying(clearAutomatically = true)
	@Query("update UserDetail ud set ud.urlFoto =:urlFoto where ud.idUserDetail =:idUserDetail")
	public void updateFotoProfilUrl(@Param("idUserDetail") int idUserDetail, @Param("urlFoto") String urlFoto);
	
	@Modifying(clearAutomatically = true)
	@Query("update UserDetail ud set ud.urlIdentitas =:urlIdentitas, ud.jenisIdentitas =:jenisIdentitas where ud.idUserDetail =:idUserDetail")
	public void updateIdentitas(@Param("idUserDetail") int idUserDetail, @Param("urlIdentitas") String urlIdentitas, @Param("jenisIdentitas") String jenisIdentitas);

	@Modifying(clearAutomatically = true)
	@Query("update UserDetail ud set ud.urlIdentitas2 =:urlIdentitas2, ud.jenisIdentitas2 =:jenisIdentitas2 where ud.idUserDetail =:idUserDetail")
	public void updateIdentitas2(@Param("idUserDetail") int idUserDetail, @Param("urlIdentitas2") String urlIdentitas2, @Param("jenisIdentitas2") String jenisIdentitas2);
	
}
