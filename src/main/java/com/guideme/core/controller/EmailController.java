package com.guideme.core.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.guideme.common.model.EmailModel;
import com.guideme.core.service.inf.EmailServiceInf;
import com.guideme.core.service.inf.TokenVerifikasiInf;

@RestController
@RequestMapping("/email")
public class EmailController {
	Logger logger = Logger.getLogger(EmailController.class);
	
	@Value("${ohmyguide.email.address}")
	String ohmyguideEmailAddress;
	
	EmailServiceInf emailService;
	TokenVerifikasiInf tokenService;
	
	@Autowired
	EmailController(EmailServiceInf emailService, TokenVerifikasiInf tokenService){
		this.emailService = emailService;
		this.tokenService = tokenService;
	}
	
	@RequestMapping(value="/sendHtml", method = RequestMethod.POST)
	public void sendHtmlEmail(@RequestBody EmailModel email){
		Map<String, String> param = new HashMap<>();
		emailService.sendHtmlEmail(email,param);
	}
	
	@RequestMapping(value="/sendHtmlTest", method = RequestMethod.GET)
	public void sendHtmlEmailTest(){
		Map<String, String> param = new HashMap<>();
		
		EmailModel email = new EmailModel();
		email.setFrom(ohmyguideEmailAddress);
		email.setTo("fzdsaputra@gmail.com");
		email.setSenderName("OHMYGUIDE! Indonesia");
		email.setSubject("Konfirmasi pendaftaran berhasil");
		email.setTemplateName("verifikasi-email-pendaftaran.html");
		
		emailService.sendHtmlEmail(email,param);
	}
	
	@RequestMapping("/verifikasiEmail/{token}")
	public Map verifikasiEmail(@PathVariable("token") String token){
		return tokenService.verifyEmail(token);
	}
	
}
