package com.guideme.core.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.guideme.common.entity.ChatMessage;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.service.impl.ChatMessageServiceImpl;
import com.guideme.core.service.inf.ChatMessageServiceInf;

@RestController
@RequestMapping("/chatMessage")
public class ChatMessageController {
	private static final Logger logger = Logger.getLogger(ChatMessageController.class);
	
	ChatMessageServiceInf chatMessageServiceImpl;
	
	@Autowired
	public ChatMessageController(ChatMessageServiceImpl chatMessageServiceImpl){
		this.chatMessageServiceImpl = chatMessageServiceImpl;
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public StatusResponse saveChatMessage(@RequestBody ChatMessage chatMessage){
		StatusResponse statusResp = chatMessageServiceImpl.saveChatMessage(chatMessage);
		
		return statusResp;
	}
	
	@RequestMapping(value="/{tokenId}", method=RequestMethod.GET)
	public StatusResponse getByTokenId(@PathVariable("tokenId") String tokenId, Pageable pageable){
		StatusResponse statusResp = chatMessageServiceImpl.getByTokenPageable(tokenId, pageable);
		return statusResp;
	}
}
