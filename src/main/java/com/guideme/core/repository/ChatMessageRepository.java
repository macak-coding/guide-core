package com.guideme.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.guideme.common.entity.ChatMessage;

@Repository
public interface ChatMessageRepository extends PagingAndSortingRepository<ChatMessage, Integer>{
	public Page<ChatMessage> findByChatToken(String token, Pageable pageable);
	public Page<ChatMessage> findByPengirim(String pengirim, Pageable pageable);
	public Page<ChatMessage> findByPenerima(String penerima, Pageable pageable);
}
