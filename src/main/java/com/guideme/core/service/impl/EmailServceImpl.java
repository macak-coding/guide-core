package com.guideme.core.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.guideme.common.model.EmailModel;
import com.guideme.core.service.inf.EmailServiceInf;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
public class EmailServceImpl implements EmailServiceInf{
	private static final Logger logger = Logger.getLogger(EmailServceImpl.class);
	
	@Value("${ohmyguide.email.address}")
	String ohmyguideEmailAddress;
	
	@Value("${ohmyguide.title}")
	String ohmyguideTitle;
	
	private JavaMailSender javaMailSender;
	private FreeMarkerConfigurationFactoryBean freemarkerConfiguration;
	
	@Autowired
	EmailServceImpl(JavaMailSender javaMailSender, FreeMarkerConfigurationFactoryBean freemarkerConfiguration){
		this.javaMailSender = javaMailSender;
		this.freemarkerConfiguration = freemarkerConfiguration;
	}
	
	@Override
	public void sendPlainEmail(String from, String to, String subject, String body) {
		
	}

	@Override
	@Async
	public void sendHtmlEmail(final EmailModel email, Map<String, String> param) {
		logger.info("Sending email address to: "+email.getTo()+", Subject: "+email.getSubject()+", template: "+email.getTemplateName());
		
		Configuration config = null;
		
		try {
			config = freemarkerConfiguration.createConfiguration();
			final Template template = config.getTemplate("email-template/"+email.getTemplateName());
			final String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, param);
			
			MimeMessagePreparator preparator = new MimeMessagePreparator() {
		         public void prepare(MimeMessage mimeMessage) throws Exception {
		            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
		            message.setTo(email.getTo());
		            message.setFrom(new InternetAddress(ohmyguideEmailAddress, ohmyguideTitle));
		            message.setSubject(email.getSubject());
		            message.setText(html, true);
		         }
		      };
			
			javaMailSender.send(preparator);
		} catch (IOException | TemplateException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		logger.info("Sending email to "+email.getTo()+" success!");;
		
	}

}
