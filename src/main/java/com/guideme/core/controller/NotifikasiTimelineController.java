package com.guideme.core.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.guideme.common.entity.NotifikasiTimeline;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.service.inf.NotifikasiTimelineInf;

@RestController
@RequestMapping("/notifikasiTimeline")
public class NotifikasiTimelineController {
	private static final Logger logger = Logger.getLogger(NotifikasiTimelineController.class);
	
	private NotifikasiTimelineInf notifikasiTimelineService;
	
	@Autowired
	public NotifikasiTimelineController(NotifikasiTimelineInf notifikasiTimelineService){
		this.notifikasiTimelineService = notifikasiTimelineService;
	}
	
	@RequestMapping(value="/getByIdPerjalanan/{idPerjalanan}", method=RequestMethod.GET)
	StatusResponse<List<NotifikasiTimeline>> getByIdPerjalanan(@PathVariable("idPerjalanan") int idPerjalanan){
		
		StatusResponse statusResponse = notifikasiTimelineService.getNotifikasiTimelineByIdPerjalanan(idPerjalanan);
		
		return statusResponse;
	}
	
	@RequestMapping(value="/approvePembayaran/{idTransaksi}", method=RequestMethod.PUT)
	StatusResponse<List<NotifikasiTimeline>> approvePembayaran(@PathVariable("idTransaksi") int idTransaksi){
		
		StatusResponse statusResponse = notifikasiTimelineService.approvePembayaran(idTransaksi);
		
		return statusResponse;
	}
	
	@RequestMapping(value="/selesaikanPembayaran/{idTransaksi}", method=RequestMethod.PUT)
	StatusResponse<List<NotifikasiTimeline>> selesaikanPembayaran(@PathVariable("idTransaksi") int idTransaksi){
		
		StatusResponse statusResponse = notifikasiTimelineService.selesaikanPembayaran(idTransaksi);
		
		return statusResponse;
	}
	
	@RequestMapping(value="/batalkanPembayaran/{idTransaksi}", method=RequestMethod.PUT)
	StatusResponse<List<NotifikasiTimeline>> batalkanPembayaran(@PathVariable("idTransaksi") int idTransaksi){
		
		StatusResponse statusResponse = notifikasiTimelineService.batalkanPembayaran(idTransaksi);
		
		return statusResponse;
	}
	
	@RequestMapping(value="/ulangiPembayaran/{idTransaksi}", method=RequestMethod.PUT)
	StatusResponse<List<NotifikasiTimeline>> ulangiPembayaran(@PathVariable("idTransaksi") int idTransaksi){
		
		StatusResponse statusResponse = notifikasiTimelineService.ulangiPembayaran(idTransaksi);
		
		return statusResponse;
	}
}
