package com.guideme.core.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.guideme.common.entity.EmailTemplate;
import com.guideme.common.entity.JalanBareng;

@Repository
public interface EmailTemplateRepository extends PagingAndSortingRepository<EmailTemplate, Integer>{
	EmailTemplate findByKodeEmailTemplate(String kodeEmailTemplate);
}
