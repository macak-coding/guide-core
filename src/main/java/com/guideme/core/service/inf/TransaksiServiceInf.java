package com.guideme.core.service.inf;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.guideme.common.entity.Transaksi;
import com.guideme.common.model.StatusResponse;

public interface TransaksiServiceInf {
	Page<Transaksi> getListTransaksiPageable(Pageable pageable);
	StatusResponse getListTransaksiForAdmin();
	StatusResponse updateBuktiTransaksi(int idTransaksi, String statusPembayaran, String urlBuktiPembayaran);
	StatusResponse getByIdTransaksi(String kodeTransaksi, String username);
	StatusResponse getByIdPerjalanan(int idPerjalanan);
}
