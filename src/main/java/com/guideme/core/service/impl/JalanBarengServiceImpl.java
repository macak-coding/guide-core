package com.guideme.core.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guideme.common.entity.EmailTemplate;
import com.guideme.common.entity.JalanBareng;
import com.guideme.common.entity.JalanBarengAnggota;
import com.guideme.common.entity.TokenVerifikasi;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.entity.ViewSearchJalanBareng;
import com.guideme.common.model.EmailModel;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.repository.EmailTemplateRepository;
import com.guideme.core.repository.JalanBarengAnggotaRepository;
import com.guideme.core.repository.JalanBarengRepository;
import com.guideme.core.repository.UserDetailRepository;
import com.guideme.core.repository.ViewSearchJalanBarengRepository;
import com.guideme.core.service.inf.EmailServiceInf;
import com.guideme.core.service.inf.JalanBarengServiceInf;

@Service
public class JalanBarengServiceImpl implements JalanBarengServiceInf{
	Logger logger = Logger.getLogger(JalanBarengServiceImpl.class);
	
	@Value("${ohmyguide.gui.server.address}")
	String GUI_SERVER_ADDRESS;
	
	private EmailServiceInf emailService;
	private JalanBarengRepository jalanBarengRepo;
	private JalanBarengAnggotaRepository jalanBarengAnggotaRepo;
	private EmailTemplateRepository emailTemplateRepo;
	private UserDetailRepository userDetailRepo;
	private ViewSearchJalanBarengRepository viewSearchJalanBbarengRepo;
	
	@Autowired
	public JalanBarengServiceImpl(EmailServiceInf emailService, JalanBarengRepository jalanBarengRepo, 
			EmailTemplateRepository emailTemplateRepo, UserDetailRepository userDetailRepo,
			JalanBarengAnggotaRepository jalanBarengAnggotaRepo, ViewSearchJalanBarengRepository viewSearchJalanBbarengRepo){
		this.emailService = emailService;
		this.jalanBarengRepo = jalanBarengRepo;
		this.emailTemplateRepo = emailTemplateRepo;
		this.userDetailRepo = userDetailRepo;
		this.jalanBarengAnggotaRepo = jalanBarengAnggotaRepo;
		this.viewSearchJalanBbarengRepo = viewSearchJalanBbarengRepo;
	}
	
	@Override
	@Transactional
	public StatusResponse<String> createJalanBareng(JalanBareng jalanBareng) {
		String tokenId = UUID.randomUUID().toString();
		
		try {
			jalanBareng.setTokenId(tokenId);
			jalanBareng.setStatusPerjalanan("BARU");
			jalanBareng.setCreatedOn(new Date());
			jalanBarengRepo.save(jalanBareng);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[CORE SERVICE] Gagal menyimpan event Jalan Bareng, id_user_detail: "+jalanBareng.getIdPembuat());
			return new StatusResponse("failed", "Gagal menyimpan event Jalan Bareng", e.getMessage());
		}
		
		try {
			UserDetail userDetail = userDetailRepo.findByUsername(jalanBareng.getIdPembuat().getIdUser().getUsername());
			EmailTemplate emailTemplate = emailTemplateRepo.findByKodeEmailTemplate("JLB001");

			Map<String, String> param = new HashMap<>();
			param.put("urlJalanBareng", GUI_SERVER_ADDRESS + "/travel-partner/chat");
			param.put("tokenId", tokenId);
			param.put("namaLengkap", userDetail.getNamaLengkap());

			EmailModel emailModel = new EmailModel();
			emailModel.setTo(jalanBareng.getIdPembuat().getIdUser().getEmail());
			emailModel.setSubject(emailTemplate.getSubject());
			emailModel.setTemplateName(emailTemplate.getNamaTemplate());

			emailService.sendHtmlEmail(emailModel, param);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[CORE SERVICE] Gagal membuat dan mengirim email untuk event Jalan Bareng, email: "+jalanBareng.getIdPembuat().getIdUser().getEmail());
			return new StatusResponse("failed", "Gagal membuat dan mengirim email untuk event Jalan Bareng", e.getMessage());
		}
		
		String urlJalanBareng = GUI_SERVER_ADDRESS+"/travel-partner/chat/"+tokenId;
		return new StatusResponse("success", "Berhasil membuat event Jalan Bareng", urlJalanBareng);
	}
	
	@Override
	@Transactional
	public StatusResponse selesaiJalanBareng(JalanBareng jalanBareng) {
		try {
			jalanBarengRepo.updateStatusJalanBareng(jalanBareng.getIdJalanBareng(), "SELESAI");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[CORE SERVICE] Gagal menyelesaikan jalan bareng dengan id: "+jalanBareng.getIdJalanBareng());
			return new StatusResponse("failed", "Gagal menyelesaikan jalan bareng", e.getMessage());
		}
		return new StatusResponse("success", "Berhasil menyelesaikan jalan bareng");
	}
	
	@Override
	@Transactional
	public StatusResponse addAnggotaJalanBareng(JalanBarengAnggota jalanBarengAnggota) {
		
		UserDetail anggota = userDetailRepo.findByUsername(jalanBarengAnggota.getIdAnggota().getIdUser().getUsername());
		
		try {
			jalanBarengAnggota.setIdAnggota(anggota);
			jalanBarengAnggota.setStatus("PENDING");
			jalanBarengAnggotaRepo.save(jalanBarengAnggota);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[CORE SERVICE] Gagal menambah anggota jalan bareng dengan id: "+jalanBarengAnggota.getIdAnggota().getIdUserDetail());
			return new StatusResponse("failed", "Gagal menambah anggota jalan bareng", e.getMessage());
		}
		return new StatusResponse("success", "Berhasil menambah anggota jalan bareng");
	}
	
	@Override
	@Transactional(readOnly=true)
	public StatusResponse<JalanBareng> getByTokenID(String tokenId) {
		JalanBareng jalanBareng = null;
		
		try {
			jalanBareng = jalanBarengRepo.findByTokenId(tokenId);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[CORE SERVICE] Gagal mendapatkan data jalan bareng token: "+tokenId+", msg: "+e.getMessage());
			new StatusResponse<>("failed", "Gagal mendapatkan data jalan bareng token: "+tokenId, e.getMessage());
		}
		
		return new StatusResponse<JalanBareng>("success", "Berhasil mendapatkan data jalan bareng, token: "+tokenId, jalanBareng);
	}

	@Override
	@Transactional(readOnly=true)
	public StatusResponse getAnggotaByUsername(String username, String token) {
		JalanBarengAnggota jalanBarengAnggota = null;
		
		try {
			jalanBarengAnggota = jalanBarengAnggotaRepo.findByUsername(username, token);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[CORE SERVICE] Gagal mendapatkan data JalanBarengAnggota username: "+username+", msg: "+e.getMessage());
			new StatusResponse<>("failed", "Gagal mendapatkan data JalanBarengAnggota username: "+username, e.getMessage());
		}
		
		return new StatusResponse<JalanBarengAnggota>("success", "Berhasil mendapatkan data JalanBarengAnggota username: "+username, jalanBarengAnggota);
	}
	
	@Override
	@Transactional
	public StatusResponse acceptAngotaJalanBareng(int idAnggota, int idJalanBareng) {
		try {
			jalanBarengAnggotaRepo.updateStatusAnggota(idAnggota, idJalanBareng, "DITERIMA");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[CORE SERVICE] Gagal menerima anggota jalan bareng dengan id: "+idAnggota);
			return new StatusResponse("failed", "Gagal menerima anggota jalan bareng", e.getMessage());
		}
		return new StatusResponse("success", "Berhasil menerima anggota jalan bareng");
	}
	
	@Override
	@Transactional
	public StatusResponse rejectAngotaJalanBareng(int idAnggota, int idJalanBareng) {
		try {
			jalanBarengAnggotaRepo.updateStatusAnggota(idAnggota, idJalanBareng, "DITOLAK");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[CORE SERVICE] Gagal menolak anggota jalan bareng dengan id: "+idAnggota);
			return new StatusResponse("failed", "Gagal menolak anggota jalan bareng", e.getMessage());
		}
		return new StatusResponse("success", "Berhasil menolak anggota jalan bareng");
	}

	@Override
	@Transactional
	public StatusResponse kickAngotaJalanBareng(int idAnggota, int idJalanBareng) {
		try {
			jalanBarengAnggotaRepo.updateStatusAnggota(idAnggota, idJalanBareng, "KICKED");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[CORE SERVICE] Gagal kick anggota jalan bareng dengan id: "+idAnggota);
			return new StatusResponse("failed", "Gagal kick anggota jalan bareng", e.getMessage());
		}
		return new StatusResponse("success", "Berhasil kick anggota jalan bareng");
	}

	@Override
	public StatusResponse getAllPage(Pageable pageable) {
		Page<ViewSearchJalanBareng> pageJalanBareng = null;
		
		try {
			pageJalanBareng = viewSearchJalanBbarengRepo.findAll(pageable);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[CORE SERVICE] Gagal meminta list semua jalan bareng pagable, page: "+pageable.getPageNumber());
			return new StatusResponse("failed","[CORE SERVICE] Gagal meminta list semua jalan bareng pagable, page: "+pageable.getPageNumber(), e.getMessage());
		}
		return new StatusResponse("success", "[CORE SERVICE] Gagal meminta list semua jalan bareng pagable, page: "+pageable.getPageNumber(), pageJalanBareng);
	}

	@Override
	public StatusResponse getAllByKotaPage(String kotaTujuan, Pageable pageable) {
		Page<ViewSearchJalanBareng> pageJalanBareng = null;
		
		try {
			pageJalanBareng = viewSearchJalanBbarengRepo.findByKotaTujuanContainingIgnoreCase(kotaTujuan, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[CORE SERVICE] Gagal meminta list semua jalan bareng pagable, tujuan: "+kotaTujuan);
			return new StatusResponse("failed","[CORE SERVICE] Gagal meminta list semua jalan bareng pagable, ujuan: "+kotaTujuan, e.getMessage());
		}
		return new StatusResponse("success", "[CORE SERVICE] Berhasil meminta list semua jalan bareng pagable, tujuan: "+kotaTujuan, pageJalanBareng);
	}

}
