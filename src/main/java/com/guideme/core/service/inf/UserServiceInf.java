package com.guideme.core.service.inf;

import com.guideme.common.entity.User;
import com.guideme.common.model.StatusResponse;

public interface UserServiceInf {
	public User getUserByUsername(String username);
	public void saveUser(User user);
	public void updateEmail(String email, int idUser) throws Exception;
	public void updatePassword(String password, int idUser);
	public StatusResponse checkUsername(String username);
	public StatusResponse checkEmail(String email);
	public StatusResponse verifyUser(int idUser);
	public StatusResponse unverifyUser(int idUser);
	public StatusResponse suspendUser(int idUser);
	public StatusResponse checkPassword(String username, String password);
	public StatusResponse getUserByUsernameOrEmail(String username, String email);
}
