package com.guideme.core.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.guideme.common.entity.Perjalanan;
import com.guideme.common.entity.RatingPerjalanan;
import com.guideme.common.entity.Review;
import com.guideme.common.entity.User;
import com.guideme.common.entity.UserDetail;

@Repository
public interface PerjalananRepository extends PagingAndSortingRepository<Perjalanan, Integer>{
	
	@Query("select perj from Perjalanan perj where perj.idGuide.idUserDetail = :idGuide and perj.idTraveler.idUserDetail = :idTraveler "
			+ "and perj.statusPerjalanan = :statusPerjalanan and perj.idReview.statusReview = :statusReview")
	public Perjalanan findPerjalananForReview(@Param("idGuide") int idGuide, @Param("idTraveler") int idTraveler, 
			@Param("statusPerjalanan") String statusPerjalanan, @Param("statusReview") String statusReview);
	
	@Modifying(clearAutomatically = true)
	@Query("update Perjalanan perj set perj.statusPerjalanan = :statusPerjalanan where perj.idPerjalanan = :idPerjalanan")
	public void updateStatusPerjalanan(@Param("idPerjalanan") int idPerjalanan, @Param("statusPerjalanan") String statusPerjalanan);
	
	@Query("select perj from Perjalanan perj where perj.idGuide.idUserDetail = :idGuide order by perj.idPerjalanan desc")
	public List<Perjalanan> findByIdGuide(@Param("idGuide") int idGuide);
	
	@Query("select perj from Perjalanan perj where perj.idTraveler.idUserDetail = :idTraveler")
	public List<Perjalanan> findByIdTraveler(@Param("idTraveler") int idTraveler);
	
	public Perjalanan findByIdPerjalanan(int idPerjalanan);
	
	public Perjalanan findByIdRatingPerjalananAndIdReview(RatingPerjalanan idRatingPerjalanan, Review idReview);
	
	@Query("select perj from Perjalanan perj where perj.idGuide.idUserDetail = :idGuide order by perj.idPerjalanan desc")
	public Page<Perjalanan> findByIdGuide(@Param("idGuide") int idGuide, Pageable pageable);
	
	@Query("select perj from Perjalanan perj where perj.idTraveler.idUserDetail = :idTraveler order by perj.idPerjalanan desc")
	public Page<Perjalanan> findByIdTrveler(@Param("idTraveler") int idTraveler, Pageable pageable);
}
