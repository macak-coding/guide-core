package com.guideme.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.guideme.common.entity.Kota;
import com.guideme.common.entity.RatingDetail;
import com.guideme.common.entity.Review;
import com.guideme.common.entity.UserDetail;

@Repository
public interface RatingDetailRepository extends PagingAndSortingRepository<RatingDetail, Integer>{
	
}
