package com.guideme.core.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guideme.common.entity.TokenVerifikasi;
import com.guideme.common.entity.User;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.entity.ViewDtUserDetail;
import com.guideme.common.model.SearchCriteria;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.helper.UserDetailSpecificationBuilder;
import com.guideme.core.repository.TokenVerifikasiRepository;
import com.guideme.core.repository.UserDetailRepository;
import com.guideme.core.repository.UserRepository;
import com.guideme.core.repository.ViewDtUserDetailRepository;
import com.guideme.core.service.exception.EmailAlreadyExistException;
import com.guideme.core.service.exception.UserAlreadyExistException;
import com.guideme.core.service.inf.EmailServiceInf;
import com.guideme.core.service.inf.TokenVerifikasiInf;
import com.guideme.core.service.inf.UserDetailServiceInf;

@Service
@Transactional(readOnly=true)
public class UserDetailServiceImpl implements UserDetailServiceInf{
	Logger logger = Logger.getLogger(UserDetailServiceImpl.class);
	
	UserDetailRepository userDetailRepo;
	UserRepository userRepository;
	TokenVerifikasiInf tokenVerifikasiService;
	ViewDtUserDetailRepository viewDtUserDetailRepo;
	
	@Autowired
	UserDetailServiceImpl(UserDetailRepository userDetailRepo, UserRepository userRepository,
						EmailServiceInf emailService, TokenVerifikasiInf tokenVerifikasiService,
						ViewDtUserDetailRepository viewDtUserDetailRepo){
		this.userDetailRepo = userDetailRepo;
		this.userRepository = userRepository;
		this.tokenVerifikasiService = tokenVerifikasiService;
		this.viewDtUserDetailRepo = viewDtUserDetailRepo;
	}
	
	@Override
	@Transactional(readOnly=false)
	public StatusResponse getUserDetailByUsername(String username) {
		UserDetail userDetail = null;
		
		try {
			userDetail = userDetailRepo.findByUsername(username);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new StatusResponse("Failed", "[CORE] Error saat mengambil detail user: "+username,e.getMessage());
		}
		
		return new StatusResponse("success","Berhasil mendapatkan user "+username,userDetail);
	}
	
	@Override
	@Transactional(readOnly=false)
	public StatusResponse saveUserDetail(UserDetail userDetail){
		User userByEmail = userRepository.findByEmail(userDetail.getIdUser().getEmail());
		User userByUsername = userRepository.findByUsername(userDetail.getIdUser().getUsername());
		
		logger.debug("########## Total Rating: "+userDetail);
		
		if(null == userByEmail){
			if(null == userByUsername){
				try {
					// Role guide dan traveler harus uppercase
					userDetail.getIdUser().setUserRoles(userDetail.getIdUser().getUserRoles().toUpperCase());
					
					userDetailRepo.save(userDetail);
					
					String tokenCode = tokenVerifikasiService.createAndSaveEmailToken(userDetail.getIdUser().getEmail());
					
					return new StatusResponse("success", "User berhasil disimpan", tokenCode);
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("[SERVICE CORE] saveUserDetail() "+e.getMessage());
					return new StatusResponse("failed", "User gagal disimpan",e.getMessage());
				}
			}else{
				return new StatusResponse("failed", "User "+userDetail.getIdUser().getUsername()+" telah terdaftar.");
			}
		}else{
			return new StatusResponse("failed", "Email "+userDetail.getIdUser().getEmail()+" telah terdaftar.");
		}
	}

	@Override
	@Transactional(readOnly=false)
	public StatusResponse updateUserStatus(int idUserDetail, String newStatus){
		try {
			userDetailRepo.updateUserStatus(idUserDetail, newStatus);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new StatusResponse("failed","[CORE] Update status user gagal", e.getMessage());
		}
		return new StatusResponse("success", "Status user berhasil diupdate.");
	} 

	@Override
	public StatusResponse getAllUserDetailPageable(Pageable pageable) {
		Page<UserDetail> page = null;
		
		try {
			page = userDetailRepo.findAll(pageable);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new StatusResponse("failed","[CORE] Gagal mengambil data user detail.",e.getMessage());
		}
		
		return new StatusResponse("success","Berhasil mengambil data user detail.", page);
	}
	
	@Override
	public StatusResponse getAllUserDetailForAdmin() {
		Iterable<ViewDtUserDetail> listUserDetail = null;
		
		try {
			listUserDetail = viewDtUserDetailRepo.findAll();
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new StatusResponse("failed","[CORE] Gagal mengambil data user detail.",e.getMessage());
		}
		
		return new StatusResponse("success","Berhasil mengambil data user detail.", listUserDetail);
	}

	@Override
	public StatusResponse<Page<UserDetail>> getUserDetailByCriteria(Pageable pageable, String searchString) {
		UserDetailSpecificationBuilder builder = new UserDetailSpecificationBuilder();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Page<UserDetail> page = null;
		
		try {
        	//Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
			Pattern pattern = Pattern.compile("([\\w\\.]+?)(:|<|>)([\\w\\-]+),");
            Matcher matcher = pattern.matcher(searchString + ",");
            while (matcher.find()) {
            	logger.info("Search string: "+matcher.group(1)+" "+matcher.group(2)+" "+matcher.group(3));
            	
            	if((matcher.group(1).contains("tanggal"))) /* Kalo key nya tangggal, pradicatenya beda */
            		builder.with(matcher.group(1), matcher.group(2), sdf.parse(matcher.group(3)));
            	else
            		builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
            }
             
            Specification<UserDetail> spec = builder.build();
            page = userDetailRepo.findAll(spec, pageable);
		} catch (Exception e) {
			logger.error("EXCEPTION: "+e.getMessage());
			return new StatusResponse("failed","[CORE] Terjadi error di service ecore.", e.getMessage());
		}
        
        return new StatusResponse("success","Success search by criteria", page);
	}

	@Override
	public StatusResponse getUserDetailById(int idUserDetail) {
		UserDetail userDetail = null;
		
		try {
			userDetail = userDetailRepo.findByIdUserDetail(idUserDetail);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new StatusResponse("failed", "[CORE] Error saat mengambil detail user ID: "+idUserDetail,e.getMessage());
		}
		
		return new StatusResponse("success","Berhasil mendapatkan user "+idUserDetail, userDetail);
	}

	@Override
	@Transactional(readOnly=false)
	public StatusResponse updateFotoProfilUrl(int idUserDetail, String url) {
		try {
			userDetailRepo.updateFotoProfilUrl(idUserDetail, url);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return new StatusResponse("failed","[SERVICE CORE] Update url foto user gagal", e.getMessage());
		}
		return new StatusResponse("success", "Foto profil berhasil diupdate.");
	}

	@Override
	@Transactional(readOnly=false)
	public StatusResponse updateFotoIdentitas(UserDetail userDetail, int flagIdentitas) {
		try {
			User userVerif = userRepository.findOne(userDetail.getIdUser().getIdUser());
			
			if(flagIdentitas == 1){
				userDetailRepo.updateIdentitas(userDetail.getIdUserDetail(), userDetail.getUrlIdentitas(), userDetail.getJenisIdentitas());
				
				if(userVerif.getStatusVerifikasi().equalsIgnoreCase("UNVERIFIED"))
					userRepository.updateStatusVerifikasi("FINISHED1", userDetail.getIdUser().getIdUser());
				else if(userVerif.getStatusVerifikasi().equalsIgnoreCase("FINISHED2"))
					userRepository.updateStatusVerifikasi("PENDING", userDetail.getIdUser().getIdUser());
			}else{
				userDetailRepo.updateIdentitas2(userDetail.getIdUserDetail(), userDetail.getUrlIdentitas2(), userDetail.getJenisIdentitas2());
				
				if(userVerif.getStatusVerifikasi().equalsIgnoreCase("UNVERIFIED"))
					userRepository.updateStatusVerifikasi("FINISHED2", userDetail.getIdUser().getIdUser());
				else if(userVerif.getStatusVerifikasi().equalsIgnoreCase("FINISHED1"))
					userRepository.updateStatusVerifikasi("PENDING", userDetail.getIdUser().getIdUser());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return new StatusResponse("failed","[SERVICE CORE] Update url foto identitas user gagal", e.getMessage());
		}
		return new StatusResponse("success", "Foto identitas berhasil diupdate.");
	}

	@Override
	@Transactional(readOnly=false)
	public StatusResponse updateUserDetail(UserDetail userDetail) {
		
		if (null != userDetail.getTarif()) {
			// Tarif yang ditampilkan = tarif + 10%
			BigDecimal tarif = userDetail.getTarif();
			BigDecimal tarif10Persen = tarif.multiply(new BigDecimal("10")).divide(new BigDecimal("100"));
			BigDecimal tarifDitampilkan = tarif.add(tarif10Persen);

			userDetail.setTarifDitampilkan(tarifDitampilkan);
		}
		
		try {
			userRepository.updateStatusProfileById("LENGKAP", userDetail.getIdUser().getIdUser());
			userDetailRepo.save(userDetail);
		} catch (Exception e) {
			
			e.printStackTrace();
			logger.error("[SERVICE CORE] error updating user updateUserDetail() " + e.getMessage());
			return new StatusResponse("failed", "User gagal diupdate, idUsreDetail: "+userDetail.getIdUserDetail(), e.getMessage());
			
		}

		return new StatusResponse("success", "User berhasil disimpan diupdate");
	}
}
