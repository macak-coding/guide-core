package com.guideme.core.helper;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.guideme.common.entity.RatingDetail;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.SearchCriteria;

public class UserDetailSpecificationNestedKey implements Specification<UserDetail> {

	private SearchCriteria criteria;

	public UserDetailSpecificationNestedKey(SearchCriteria criteria) {
		this.criteria = criteria;
	}

	@Override
	public Predicate toPredicate(Root<UserDetail> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
		String key = criteria.getKey();
		
		if (criteria.getOperation().equalsIgnoreCase(">")) {
			return builder.greaterThanOrEqualTo(root.<Object>get(key.split("\\.")[0]).<String>get(key.split("\\.")[1]), criteria.getValue().toString());
		} else if (criteria.getOperation().equalsIgnoreCase("<")) {
			return builder.lessThanOrEqualTo(root.<Object>get(key.split("\\.")[0]).<String>get(key.split("\\.")[1]), criteria.getValue().toString());
		} else if (criteria.getOperation().equalsIgnoreCase(":")) {
			return builder.equal(root.<Object>get(key.split("\\.")[0]).<String>get(key.split("\\.")[1]), criteria.getValue().toString());
		}

		return null;
	}

}
