package com.guideme.core.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.guideme.common.entity.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Integer>{
	
	@Modifying(clearAutomatically = true)
	@Query("update User u set u.email =:email where u.idUser =:idUser")
	public void updateEmail(@Param("email") String email, @Param("idUser") int idUser);
	
	@Modifying(clearAutomatically = true)
	@Query("update User u set u.password =:password where u.idUser =:idUser")
	public void updatePassword(@Param("password") String password, @Param("idUser") int idUser);
	
	public User findByEmail(String email);
	
	public User findByUsername(String username);
	
	public User findByUsernameAndPassword(String username, String password);
	
	public User findByUsernameOrEmail(String username, String email);
	
	@Modifying(clearAutomatically = true)
	@Query("update User u set u.statusVerifikasi =:statusVerifikasi where u.idUser =:idUser")
	public void updateStatusVerifikasi(@Param("statusVerifikasi") String statusVerifikasi, @Param("idUser") int idUser);
	
	@Modifying(clearAutomatically = true)
	@Query("update User u set u.statusAkun =:statusAkun where u.idUser =:idUser")
	public void updateStatusAkun(@Param("statusAkun") String statusAkun, @Param("idUser") int idUser);
	
	@Modifying(clearAutomatically = true)
	@Query("update User u set u.statusProfile =:statusProfile where u.idUser =:idUser")
	public void updateStatusProfile(@Param("statusProfile") String statusAkun, @Param("idUser") int idUser);
	
	@Modifying(clearAutomatically = true)
	@Query("update User u set u.statusAkun =:statusAkun where u.email =:email")
	public void updateStatusAkunByEmail(@Param("statusAkun") String statusAkun, @Param("email") String email);
	
	@Modifying(clearAutomatically = true)
	@Query("update User u set u.statusProfile =:statusProfile where u.idUser =:idUser")
	public void updateStatusProfileById(@Param("statusProfile") String statusProfile, @Param("idUser") int idUser);
}
