package com.guideme.core.service.inf;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.guideme.common.entity.ChatMessage;
import com.guideme.common.model.StatusResponse;

public interface ChatMessageServiceInf {
	public StatusResponse getByTokenPageable(String token, Pageable pageable);
	public StatusResponse saveChatMessage(ChatMessage chatMessage);
	public StatusResponse getByPengirim(String pengirim, Pageable pageable);
	public StatusResponse getByPenerima(String penerima, Pageable pageable);
}
