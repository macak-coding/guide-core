package com.guideme.core.repository;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.guideme.common.entity.Transaksi;

@Repository
public interface TransaksiRepository extends PagingAndSortingRepository<Transaksi, Integer>{
	@Modifying(clearAutomatically = true)
	@Query("update Transaksi trx set trx.statusPembayaran = :statusPembayaran where trx.idTransaksi = :idTransaksi")
	public void updateStatusPembayaran(@Param("idTransaksi") int idTransaksi, @Param("statusPembayaran") String statusPembayaran);
	
	@Modifying(clearAutomatically = true)
	@Query("update Transaksi trx set trx.statusPembayaran = :statusPembayaran, trx.urlBuktiPembayaran = :urlBuktiPembayaran where trx.idTransaksi = :idTransaksi")
	public void updateBuktiPembayaran(@Param("idTransaksi") int idTransaksi, @Param("statusPembayaran") String statusPembayaran, @Param("urlBuktiPembayaran") String urlBuktiPembayaran);
	
	public Transaksi findByIdPerjalanan(int idPerjalanan);
	
	public Transaksi findByKodeTransaksiAndCreatedBy(String kodeTransaksi, String createdBy);
}
