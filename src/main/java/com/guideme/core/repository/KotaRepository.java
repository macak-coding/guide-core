package com.guideme.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.guideme.common.entity.Kota;
import com.guideme.common.entity.Notifikasi;
import com.guideme.common.entity.UserDetail;

@Repository
public interface KotaRepository extends PagingAndSortingRepository<Kota, Integer>{
	public List<Kota> findByNamaKotaContainingIgnoreCase(String namaKota);
}
