package com.guideme.core.helper;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.SearchCriteria;

public class UserDetailSpecificationDate implements Specification<UserDetail> {

	private SearchCriteria criteria;

	public UserDetailSpecificationDate(SearchCriteria criteria) {
		this.criteria = criteria;
	}

	@Override
	public Predicate toPredicate(Root<UserDetail> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

		if (criteria.getOperation().equalsIgnoreCase(">")) {
			return builder.greaterThanOrEqualTo(root.<Date> get(criteria.getKey()), (Date) criteria.getValue());
		} else if (criteria.getOperation().equalsIgnoreCase("<")) {
			return builder.lessThanOrEqualTo(root.<Date> get(criteria.getKey()), (Date) criteria.getValue());
		} else if (criteria.getOperation().equalsIgnoreCase(":")) {
			return builder.equal(root.<Date> get(criteria.getKey()), (Date) criteria.getValue());
		}

		return null;
	}

}
