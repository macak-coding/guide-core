package com.guideme.core.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.SearchCriteria;

public class UserDetailSpecificationBuilder {
	     
	    private final List<SearchCriteria> params;
	 
	    public UserDetailSpecificationBuilder() {
	        params = new ArrayList<SearchCriteria>();
	    }
	 
	    public UserDetailSpecificationBuilder with(String key, String operation, Object value) {
	        params.add(new SearchCriteria(key, operation, value));
	        return this;
	    }
	    
	    public Specification<UserDetail> build() {
	        if (params.size() == 0) {
	            return null;
	        }
	 
	        List<Specification<UserDetail>> specs = new ArrayList<Specification<UserDetail>>();
	        for (SearchCriteria param : params) {
	        	if(param.getKey().contains("tanggal"))
	        		specs.add(new UserDetailSpecificationDate(param));
	        	else if(param.getKey().split("\\.").length > 1) /* Jika key mengandung minimal 2 kata setelah di split dengan (.) */
	        		specs.add(new UserDetailSpecificationNestedKey(param));
	        	else
	        		specs.add(new UserDetailSpecification(param));
	        }
	 
	        Specification<UserDetail> result = specs.get(0);
	        for (int i = 1; i < specs.size(); i++) {
	            result = Specifications.where(result).and(specs.get(i));
	        }
	        return result;
	    }
}
