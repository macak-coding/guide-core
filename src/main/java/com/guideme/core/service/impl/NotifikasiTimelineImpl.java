package com.guideme.core.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guideme.common.constanta.Constanta;
import com.guideme.common.entity.Notifikasi;
import com.guideme.common.entity.NotifikasiTimeline;
import com.guideme.common.entity.NotifikasiTipe;
import com.guideme.common.entity.Perjalanan;
import com.guideme.common.entity.Transaksi;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.StatusResponse;
import com.guideme.core.repository.NotifikasiRepository;
import com.guideme.core.repository.NotifikasiTimelineRepository;
import com.guideme.core.repository.PerjalananRepository;
import com.guideme.core.repository.TransaksiRepository;
import com.guideme.core.repository.UserDetailRepository;
import com.guideme.core.service.inf.NotifikasiTimelineInf;

@Service
@Transactional(readOnly=true)
public class NotifikasiTimelineImpl implements NotifikasiTimelineInf{
	Logger logger = Logger.getLogger(NotifikasiTimeline.class);
	
	private NotifikasiTimelineRepository notifikasiTimelineRepo;
	private TransaksiRepository transaksiRepo;
	private NotifikasiRepository notifikasiRepo;
	private PerjalananRepository perjalananRepo;
	
	@Autowired
	NotifikasiTimelineImpl(NotifikasiTimelineRepository notifikasiTimelineRepo, TransaksiRepository transaksiRepo,
							NotifikasiRepository notifikasiRepo, PerjalananRepository perjalananRepo){
		this.notifikasiTimelineRepo = notifikasiTimelineRepo;
		this.transaksiRepo = transaksiRepo;
		this.notifikasiRepo = notifikasiRepo;
		this.perjalananRepo = perjalananRepo;
	}
	
	@Override
	public StatusResponse<List<NotifikasiTimeline>> getNotifikasiTimelineByIdPerjalanan(int idPerjalanan) {
		
		List<NotifikasiTimeline> listNotifTimeline = null;
		
		try {
			listNotifTimeline = notifikasiTimelineRepo.findByIdPerjalananOrderByIdNotifikasiDesc(idPerjalanan);
		} catch (Exception e) {
			e.printStackTrace();
			return new StatusResponse<>("failed", "Gagal mendapat data notif timeline", e.getMessage());
		}
		
		return new StatusResponse<>("success", "Berhasil mendapat data notif timeline", listNotifTimeline);
	}
	
	@Override
	@Transactional(readOnly=false)
	public StatusResponse approvePembayaran(int idTransaksi) {
		
		try {
			Transaksi transaksi = transaksiRepo.findOne(idTransaksi);
			
			NotifikasiTimeline notifTimelinePembayaranDisetujui = new NotifikasiTimeline();
			notifTimelinePembayaranDisetujui.setCreatedOn(new Date());
			notifTimelinePembayaranDisetujui.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_PEMBAYARAN_DITERIMA));
			notifTimelinePembayaranDisetujui.setIdPerjalanan(transaksi.getIdPerjalanan());
			notifTimelinePembayaranDisetujui.setPesan(Constanta.NOTIFIKASI_PESAN_PEMBAYARAN_DITERIMA);
			notifTimelinePembayaranDisetujui.setRolePenerima("GUIDE");
			notifTimelinePembayaranDisetujui.setRolePengirim("SYSTEM");
			
			logger.debug("[CORE SERVICE] Get perjalanan, id: "+transaksi.getIdPerjalanan());
			Perjalanan perjalanan = perjalananRepo.findByIdPerjalanan(transaksi.getIdPerjalanan());
			
			Notifikasi notifPembayaranDiterima = new Notifikasi();
			notifPembayaranDiterima.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_PEMBAYARAN_DITERIMA));
			notifPembayaranDiterima.setIdPengirim(perjalanan.getIdGuide());
			notifPembayaranDiterima.setIdPenerima(perjalanan.getIdTraveler());
			notifPembayaranDiterima.setIdPerjalanan(perjalanan);
			notifPembayaranDiterima.setPesan(Constanta.NOTIFIKASI_PESAN_PEMBAYARAN_DITERIMA);
			notifPembayaranDiterima.setCreatedOn(new Date());
			
			Notifikasi notifPembayaranDiterima2 = new Notifikasi();
			notifPembayaranDiterima2.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_PEMBAYARAN_DITERIMA));
			notifPembayaranDiterima2.setIdPengirim(perjalanan.getIdTraveler());
			notifPembayaranDiterima2.setIdPenerima(perjalanan.getIdGuide());
			notifPembayaranDiterima2.setIdPerjalanan(perjalanan);
			notifPembayaranDiterima2.setPesan(Constanta.NOTIFIKASI_PESAN_PEMBAYARAN_DITERIMA);
			notifPembayaranDiterima2.setCreatedOn(new Date());
			
			logger.debug("[CORE SERVICE] Menyimpan timeline notifikasi approve pembayaran"); 
			
			notifikasiTimelineRepo.save(notifTimelinePembayaranDisetujui);
			transaksiRepo.updateStatusPembayaran(idTransaksi, "DISETUJUI");
			notifikasiRepo.save(notifPembayaranDiterima);
			notifikasiRepo.save(notifPembayaranDiterima2);
			
		} catch (Exception e) {
			e.printStackTrace();
			return new StatusResponse<>("failed", "Gagal update status pembayaran menjadi DISETUJUI, id: "+idTransaksi, e.getMessage());
		}
		
		return new StatusResponse<>("success", "SUkses update status pembayaran menjadi DISETUJUI id: "+idTransaksi);
	}
	
	@Override
	@Transactional(readOnly=false)
	public StatusResponse selesaikanPembayaran(int idTransaksi) {
		
		try {
			Transaksi transaksi = transaksiRepo.findOne(idTransaksi);
			
			NotifikasiTimeline notifTimelinePembayaranDisetujui = new NotifikasiTimeline();
			notifTimelinePembayaranDisetujui.setCreatedOn(new Date());
			notifTimelinePembayaranDisetujui.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_PEMBAYARAN_SELESAI));
			notifTimelinePembayaranDisetujui.setIdPerjalanan(transaksi.getIdPerjalanan());
			notifTimelinePembayaranDisetujui.setPesan(Constanta.NOTIFIKASI_PESAN_PEMBAYARAN_SELESAI);
			notifTimelinePembayaranDisetujui.setRolePenerima("TRAVELER");
			notifTimelinePembayaranDisetujui.setRolePengirim("SYSTEM");
			
			logger.debug("[CORE SERVICE] Get perjalanan, id: "+transaksi.getIdPerjalanan());
			Perjalanan perjalanan = perjalananRepo.findByIdPerjalanan(transaksi.getIdPerjalanan());
			
			Notifikasi notifPembayaranDiterima = new Notifikasi();
			notifPembayaranDiterima.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_PEMBAYARAN_SELESAI));
			notifPembayaranDiterima.setIdPengirim(perjalanan.getIdGuide());
			notifPembayaranDiterima.setIdPenerima(perjalanan.getIdTraveler());
			notifPembayaranDiterima.setIdPerjalanan(perjalanan);
			notifPembayaranDiterima.setPesan(Constanta.NOTIFIKASI_PESAN_PEMBAYARAN_SELESAI);
			notifPembayaranDiterima.setCreatedOn(new Date());
			
			Notifikasi notifPembayaranDiterima2 = new Notifikasi();
			notifPembayaranDiterima2.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_PEMBAYARAN_SELESAI));
			notifPembayaranDiterima2.setIdPengirim(perjalanan.getIdTraveler());
			notifPembayaranDiterima2.setIdPenerima(perjalanan.getIdGuide());
			notifPembayaranDiterima2.setIdPerjalanan(perjalanan);
			notifPembayaranDiterima2.setPesan(Constanta.NOTIFIKASI_PESAN_PEMBAYARAN_SELESAI);
			notifPembayaranDiterima2.setCreatedOn(new Date());
			
			logger.debug("[CORE SERVICE] Menyimpan timeline notifikasi selesaikan pembayaran");
			
			notifikasiTimelineRepo.save(notifTimelinePembayaranDisetujui);
			transaksiRepo.updateStatusPembayaran(idTransaksi, "SELESAI");
			notifikasiRepo.save(notifPembayaranDiterima);
			notifikasiRepo.save(notifPembayaranDiterima2);
			
		} catch (Exception e) {
			e.printStackTrace();
			return new StatusResponse<>("failed", "Gagal update status pembayaran menjadi SELESAI, id: "+idTransaksi, e.getMessage());
		}
		
		return new StatusResponse<>("success", "Sukses update status pembayaran menjadi SELESAI id: "+idTransaksi);
	}
	
	@Override
	@Transactional(readOnly=false)
	public StatusResponse batalkanPembayaran(int idTransaksi) {
		
		try {
			Transaksi transaksi = transaksiRepo.findOne(idTransaksi);
			
			NotifikasiTimeline notifTimelinePembayaranDibatalkan = new NotifikasiTimeline();
			notifTimelinePembayaranDibatalkan.setCreatedOn(new Date());
			notifTimelinePembayaranDibatalkan.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_PEMBAYARAN_DIBATALKAN));
			notifTimelinePembayaranDibatalkan.setIdPerjalanan(transaksi.getIdPerjalanan());
			notifTimelinePembayaranDibatalkan.setPesan(Constanta.NOTIFIKASI_PESAN_PEMBAYARAN_DIBATALKAN);
			notifTimelinePembayaranDibatalkan.setRolePenerima("TRAVELER");
			notifTimelinePembayaranDibatalkan.setRolePengirim("SYSTEM");
			
			logger.debug("[CORE SERVICE] Get perjalanan, id: "+transaksi.getIdPerjalanan());
			Perjalanan perjalanan = perjalananRepo.findByIdPerjalanan(transaksi.getIdPerjalanan());
			
			Notifikasi notifPembayaranDiterima = new Notifikasi();
			notifPembayaranDiterima.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_PEMBAYARAN_DIBATALKAN));
			notifPembayaranDiterima.setIdPengirim(perjalanan.getIdGuide());
			notifPembayaranDiterima.setIdPenerima(perjalanan.getIdTraveler());
			notifPembayaranDiterima.setIdPerjalanan(perjalanan);
			notifPembayaranDiterima.setPesan(Constanta.NOTIFIKASI_PESAN_PEMBAYARAN_DIBATALKAN);
			notifPembayaranDiterima.setCreatedOn(new Date());
			
			Notifikasi notifPembayaranDiterima2 = new Notifikasi();
			notifPembayaranDiterima2.setIdNotifikasiTipe(new NotifikasiTipe(Constanta.NOTIFIKASI_PEMBAYARAN_DIBATALKAN));
			notifPembayaranDiterima2.setIdPengirim(perjalanan.getIdTraveler());
			notifPembayaranDiterima2.setIdPenerima(perjalanan.getIdGuide());
			notifPembayaranDiterima2.setIdPerjalanan(perjalanan);
			notifPembayaranDiterima2.setPesan(Constanta.NOTIFIKASI_PESAN_PEMBAYARAN_DIBATALKAN);
			notifPembayaranDiterima2.setCreatedOn(new Date());
			
			logger.debug("[CORE SERVICE] Menyimpan timeline notifikasi pembatalan pembayaran");
			
			notifikasiTimelineRepo.save(notifTimelinePembayaranDibatalkan);
			transaksiRepo.updateStatusPembayaran(idTransaksi, "DIBATALKAN");
			notifikasiRepo.save(notifPembayaranDiterima);
			notifikasiRepo.save(notifPembayaranDiterima2);
			
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Gagal update status pembayaran menjadi DIBATALKAN id: "+idTransaksi);
			return new StatusResponse<>("failed", "Gagal update status pembayaran id: "+idTransaksi, e.getMessage());
		}
		
		return new StatusResponse<>("success", "Sukses update status pembayaran menjadi SELESAI id: "+idTransaksi);
	}
	
	@Override
	@Transactional(readOnly=false)
	public StatusResponse ulangiPembayaran(int idTransaksi) {
		
		try {
			transaksiRepo.updateStatusPembayaran(idTransaksi, "BARU");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Gagal update status pembayaran menjadi BATU, id: "+idTransaksi);
			return new StatusResponse<>("failed", "Gagal update status pembayaran id: "+idTransaksi, e.getMessage());
		}
		
		return new StatusResponse<>("success", "Sukses update status pembayaran menjadi SELESAI id: "+idTransaksi);
	}

}
