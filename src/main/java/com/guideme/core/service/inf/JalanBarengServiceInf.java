package com.guideme.core.service.inf;

import org.springframework.data.domain.Pageable;

import com.guideme.common.entity.JalanBareng;
import com.guideme.common.entity.JalanBarengAnggota;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.StatusResponse;

public interface JalanBarengServiceInf {
	public StatusResponse<String> createJalanBareng(JalanBareng jalanBareng);
	public StatusResponse selesaiJalanBareng(JalanBareng jalanBareng);
	public StatusResponse addAnggotaJalanBareng(JalanBarengAnggota jalanBarengAnggota);
	public StatusResponse acceptAngotaJalanBareng(int idAnggota, int idJalanBareng);
	public StatusResponse rejectAngotaJalanBareng(int idAnggota, int idJalanBareng);
	public StatusResponse kickAngotaJalanBareng(int idAnggota, int idJalanBareng);
	public StatusResponse getByTokenID(String tokenId);
	public StatusResponse getAnggotaByUsername(String username, String token);
	public StatusResponse getAllPage(Pageable pageable);
	public StatusResponse getAllByKotaPage(String kotaTujuan, Pageable pageable);
	
}
